import { getP2PhysicsBodyOf } from '../../util/util';
import PhysicsSystem from '../systems/PhysicsSystem';

export default class Entity extends Phaser.Sprite {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  protected _isPreOrUnpresenting: boolean;

  private _enabled: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, key: string, group: Phaser.Group,
    isPhysicsBodyStatic = false) {
    super(game, x, y, key);
    group.add(this);

    this._enabled = true;
    this._isPreOrUnpresenting = false;

    this.initPhysics(isPhysicsBodyStatic);
  }

  // ----------------------------------------------------------------------------------------------
  getEnabled(): boolean {
    return (this._enabled);
  }

  // ----------------------------------------------------------------------------------------------
  setEnabled(value: boolean) {
    this._enabled = value && !this._isPreOrUnpresenting;

    this.physicsBody.debug = value && PhysicsSystem.DebugBodies;

    if (value) {
      this.physicsBody.addToWorld();
    } else {
      this.physicsBody.removeFromWorld();
    }
  }

  // ----------------------------------------------------------------------------------------------
  get physicsBody(): Phaser.Physics.P2.Body {
    return (getP2PhysicsBodyOf(this));
  }

  // ==============================================================================================
  // protected
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  /**
   * Should be overridden by custom `Entity` classes that need custom physics initialization.
   */
  protected onInitPhysics(): void {
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private initPhysics(isPhysicsBodyStatic: boolean): void {
    const game = this.game;

    game.physics.p2.enable(this);

    this.physicsBody.debug = this.getEnabled() && PhysicsSystem.DebugBodies;

    if (isPhysicsBodyStatic) {
      this.physicsBody.static = true;
    }

    this.onInitPhysics();
  }
}
