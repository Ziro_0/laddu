import Entity from './Entity';
import FloatingEffect from './effects/FloatingEffect';
import PathFollowerEffect from './effects/PathFollowerEffect';
import DurationEffect from './effects/DurationEffect';
import { CoinEffects, CoinIds, getCoinTexture } from './data/CoinData';
import IPath from './data/IPath';

export default class Coin extends Entity {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly MAIN_SCALE = 1.5;

  private static readonly PRESENT_SCALE = 0.4;

  private _effects: CoinEffects[];

  private _floatingEffect: FloatingEffect;

  private _pathFollowerEffect: PathFollowerEffect;

  private _durationEffect: DurationEffect;

  private _id: CoinIds;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, group: Phaser.Group,
    coinId: CoinIds, effects: CoinEffects[], normalDurationMs: number, warningDurationMs: number,
    path: IPath) {
    super(game, 0, 0, 'present-coin-effect', group);

    this._id = coinId;
    this._effects = effects.concat();

    this._floatingEffect = new FloatingEffect(this);
    this._pathFollowerEffect = new PathFollowerEffect(this, path);

    this._durationEffect = new DurationEffect(this, normalDurationMs, warningDurationMs);
    this._durationEffect.onComplete.addOnce(() => {
      this.destroy();
    })

    this.spawn();
  }

  // ----------------------------------------------------------------------------------------------
  get coinId(): CoinIds {
    return (this._id);
  }

  // ----------------------------------------------------------------------------------------------
  static Create(game: Phaser.Game, group: Phaser.Group, coinId: CoinIds, effects: CoinEffects[],
    normalDurationMs: number, warningDurationMs: number, path: IPath): Coin {
    return (new Coin(game, group, coinId, effects, normalDurationMs, warningDurationMs, path));
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    this._floatingEffect.stop();
    this._pathFollowerEffect.stop();
    this._durationEffect.stop();
    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  get effects(): CoinEffects[] {
    return (this._effects);
  }

  // ----------------------------------------------------------------------------------------------
  get id(): CoinIds {
    return (this._id);
  }

  // ----------------------------------------------------------------------------------------------
  postUpdate(): void {
    super.postUpdate();
    this._floatingEffect.update();
  }

  // ----------------------------------------------------------------------------------------------
  unpresent(): void {
    this.present()
      .then(() => {
        this.destroy();
        return Promise.resolve();
      })
      .catch((error) => {
        console.warn(error);
      });
  }

  // ==============================================================================================
  // protected
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  protected onInitPhysics(): void {
    this.physicsBody.kinematic = true;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private present(): Promise<void> {
    this._isPreOrUnpresenting = true;
    this.setEnabled(false);

    this.loadTexture('present-coin-effect');
    this.setScale(Coin.PRESENT_SCALE);

    this.animations.add('present', [0, 0, 1, 1, 2, 2, 3, 3]);
    this.animations.play('present', 24, false);

    return new Promise((resolve) =>
      {
        this.events.onAnimationComplete.addOnce(() => {
          this._isPreOrUnpresenting = false;
          resolve();
        });
      }
    );
  }

  // ----------------------------------------------------------------------------------------------
  private setScale(value: number): void {
    this.scale.set(value * Coin.MAIN_SCALE);
  }

  // ----------------------------------------------------------------------------------------------
  private setCoinAnimation(): void {
    this.setScale(1.0);

    const key = getCoinTexture(this.coinId);
    this.loadTexture(key);

    this.setEnabled(true);
    this.physicsBody.setRectangleFromSprite(this);

    this.physicsBody.data.shapes.forEach((shape) => {
      shape.sensor = true;
    });

    this.animations.add(key, [0, 0, 1, 1, 2, 2, 1, 1]);
    this.animations.play(key, 24, true);
  }

  // ----------------------------------------------------------------------------------------------
  private spawn() {
    this.present()
      .then(() => {
        this.setCoinAnimation();
        this._floatingEffect.start();
        this._pathFollowerEffect.start();
        this._durationEffect.start();
      })
      .catch((error) => {
        console.warn(error);
      });
  }
}
