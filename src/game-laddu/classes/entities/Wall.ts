import Entity from './Entity';

export default class Wall extends Entity {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly MORPH_PRESENT_DURATION_MS = 200;

  private _onPresent: Phaser.Signal;

  private _viewOffset: number;

  private _isOnLeft: boolean;

  private _isSpiked: boolean;

  private _isSpikedPending: boolean;

  private _isMorphing: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, isOnLeft: boolean, viewOffset: number, group: Phaser.Group) {
    super(game, 0, 0, 'wall', group, true);

    this._onPresent = new Phaser.Signal();

    this._viewOffset = viewOffset;

    this._isOnLeft = isOnLeft;
    this._isSpiked = false;

    this.updateRotation();
    this.updateTexture();

    this.physicsBody.x = this.calcPresentingX();
    this.physicsBody.y = this.height / 2 + this._viewOffset;
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    this._onPresent.dispose();
    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  get isSpiked(): boolean {
    return (this._isSpiked);
  }

  // ----------------------------------------------------------------------------------------------
  get isSpikedPending(): boolean {
    return (this._isSpikedPending);
  }

  // ----------------------------------------------------------------------------------------------
  /**
   * Morphs a wall to become spiked or unspiked.
   * @param isSpiked Specifies if the wall should be spiked or not. If `undefined`, there is
   * a 50/50 chance that the wall will switch its current state.
   * @returns `true` only if the wall is going to be spiked, _not_ if it's already spiked.
   */
  morph(isSpiked?: boolean): boolean {
    if (this._isMorphing) {
      return (this._isSpikedPending);
    }

    this._isSpikedPending = isSpiked !== undefined
      ? isSpiked
      : Math.random() >= 0.5;

    if (this._isSpikedPending === this._isSpiked) {
      return (false);
    }

    this._isMorphing = true;

    this.unpresent()
      .then(() => {
        this._isSpiked = this._isSpikedPending;
        this.updateTexture();
        this.updateRotation();
        return Promise.resolve();
      })
      .then(() => {
        return this.present();
      })
      .then(() => {
        this._isMorphing = false;
        return Promise.resolve();
      })
      .catch((error) => {
        console.warn(error);
      });

    return (this._isSpikedPending);
  }

  // ----------------------------------------------------------------------------------------------
  get onPresent(): Phaser.Signal {
    return (this._onPresent);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private calcPresentingX(): number {
    if (this.isSpiked) {
      if (this._isOnLeft) {
        return (this.width / 2);
      } else {
        return (this.game.width - this.width / 2);
      }
    }

    if (this._isOnLeft) {
      return (0);
    }

    return (this.game.width);
  }

  // ----------------------------------------------------------------------------------------------
  private calcUnpresentingX(): number {
    if (this._isOnLeft) {
      return (-this.width / 2);
    }

    return this.game.width + this.width / 2;
  }

  // ----------------------------------------------------------------------------------------------
  private present(): Promise<void> {
    this._onPresent.dispatch(this.isSpiked, this);

    const tween = this.game.tweens.create(this.physicsBody)
      .to(
        {
          x: this.calcPresentingX(),
        },
        Wall.MORPH_PRESENT_DURATION_MS,
        Phaser.Easing.Cubic.Out,
        true,
      );

    return (new Promise((resolve) => {
      tween.onComplete.addOnce(() => {
        resolve();
      });
    }));
  }

  // ----------------------------------------------------------------------------------------------
  private unpresent(): Promise<void> {
    const tween = this.game.tweens.create(this.physicsBody)
      .to(
        {
          x: this.calcUnpresentingX(),
        },
        Wall.MORPH_PRESENT_DURATION_MS,
        Phaser.Easing.Cubic.In,
        true,
      );

    return (new Promise((resolve) => {
      tween.onComplete.addOnce(() => {
        resolve();
      });
    }));
  }

  // ----------------------------------------------------------------------------------------------
  private updateRotation(): void {
    this.physicsBody.rotation = this._isOnLeft ? 0 : Math.PI;
  }

  // ----------------------------------------------------------------------------------------------
  private updateTexture(): void {
    this.loadTexture(this._isSpiked ? 'wall-spikes' : 'wall');
  }
}
