import Entity from './Entity';
import PathFollowerEffect from './effects/PathFollowerEffect';
import IPath from './data/IPath';
import AudioPlayer from '../audio/AudioPlayer';
import BumperImpactEffect from './effects/BumperImpactEffect';

export default class Bumper extends Entity {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly SPIN_DURATION_RANGE_MS = [2000, 5000];

  private static readonly PRESENT_SCALE = 0.4;

  private _pathFollowerEffect: PathFollowerEffect;

  private _audio: AudioPlayer;

  private _spinTween: Phaser.Tween;

  private _impactScaleTween: Phaser.Tween;

  private _scale: number;

  private _hits: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, group: Phaser.Group, audio: AudioPlayer, hits: number = 1,
    path?: IPath, scale = 1.0) {
    super(game, 0, 0, 'present-coin-effect', group, true);

    this._audio = audio;
    this._hits = hits;
    this._scale = scale;

    if (path) {
      this._pathFollowerEffect = new PathFollowerEffect(this, path);
    }

    this.spawn();
  }

  // ----------------------------------------------------------------------------------------------
  bump(entityJustContacted: Entity): void {
    if (!entityJustContacted) {
      return;
    }

    this._hits -= 1;

    this.playBumperSound();

    this.bumpTheOtherDude(entityJustContacted);

    this.playBumpAnimations();
  }

  // ----------------------------------------------------------------------------------------------
  static Create(game: Phaser.Game, group: Phaser.Group, audio: AudioPlayer, hits = 1,
    path?: IPath, scale = 1.0): Bumper {
    return (new Bumper(game, group, audio, hits, path, scale));
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    if (this._spinTween) {
      this._spinTween.stop();
      this._spinTween = null;
    }

    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  get hits(): number {
    return (this._hits);
  }

  // ----------------------------------------------------------------------------------------------
  set hits(value: number) {
    this._hits = value;
  }

  // ----------------------------------------------------------------------------------------------
  get isLarge(): boolean {
    return (this._scale > 1.0);
  }

  // ----------------------------------------------------------------------------------------------
  get isMoving(): boolean {
    return (this._pathFollowerEffect !== null);
  }

  // ----------------------------------------------------------------------------------------------
  unpresent(): void {
    if (this._isPreOrUnpresenting) {
      return;
    }

    this._isPreOrUnpresenting = true;

    this.present()
      .then(() => {
        this.destroy();
      })
      .catch((error) => {
        console.warn(error);
      });
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private bumpTheOtherDude(theOtherDude: Entity): void {
    const velocityScale = this.isLarge
      ? Phaser.Math.random(1.3, 1.6)
      : Phaser.Math.random(1.1, 1.25)

      theOtherDude.physicsBody.velocity.x *= velocityScale;
      theOtherDude.physicsBody.velocity.y *= velocityScale;
  }

  // ----------------------------------------------------------------------------------------------
  private playBumpAnimations(): void {
    this.playBumpAnimationsFrames();
    this.playBumpAnimationsImpacts();
    this.playBumpAnimationsScale();
  }

  // ----------------------------------------------------------------------------------------------
  private playBumpAnimationsFrames(): void {
    const FRAMES = [0, 1, 2, 3, 0, 1, 2, 3];
    this.animations.add('bump', FRAMES);

    const FPS = 24;
    this.animations.play('bump', FPS, false);

    this.events.onAnimationComplete.addOnce(() => {
      if (this._hits > 0) {
        this.animations.frame = 0;
      } else {
        this.setEnabled(false);
        this.unpresent();
      }
    });
  }

  // ----------------------------------------------------------------------------------------------
  private playBumpAnimationsImpacts(): void {
    const timer = this.game.time.create();
    timer.start();
    timer.repeat(50, 4, () => {
      BumperImpactEffect.Create(this.game, this);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private playBumpAnimationsScale(): void {
    this._impactScaleTween = this.game.tweens.create(this.scale)
      .to(
        {
          x: this._scale * 1.25,
          y: this._scale * 1.25,
        },
        200,
        Phaser.Easing.Elastic.InOut,
        true,
        0,
        0,
        true,
      )

    this._impactScaleTween.onLoop.addOnce(() => {
      this._impactScaleTween = null;
      this.scale.set(this._scale);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private playBumperSound(): void {
    if (this._scale > 1.0) {
      this._audio.play('snd-bumper-large');
    } else {
      this._audio.play('snd-bumper');
    }
  }

  // ----------------------------------------------------------------------------------------------
  private present(): Promise<void> {
    this._isPreOrUnpresenting = true;
    this.setEnabled(false);

    if (this._impactScaleTween) {
      this._impactScaleTween.stop();
      this._impactScaleTween = null
    }

    this.loadTexture('present-coin-effect');
    this.scale.set(this._scale * Bumper.PRESENT_SCALE);

    this.animations.add('present', [0, 0, 1, 1, 2, 2, 3, 3]);
    this.animations.play('present', 24, false);

    return new Promise((resolve) =>
      {
        this.events.onAnimationComplete.addOnce(() => {
          this._isPreOrUnpresenting = false;
          resolve();
        });
      }
    );
  }

  // ----------------------------------------------------------------------------------------------
  private setBumperAnimation(): void {
    this.scale.set(this._scale);

    this.loadTexture('bumper');

    this.setEnabled(true);
    this.physicsBody.setCircle(this.width / 2);

    if (Math.random() < 0.5) {
      return;
    }

    this._spinTween = this.game.tweens.create(this.physicsBody)
      .to({
        rotation: Phaser.Utils.randomChoice(1, -1) * Math.PI * 2,
      },
      Phaser.Math.random(Bumper.SPIN_DURATION_RANGE_MS[0], Bumper.SPIN_DURATION_RANGE_MS[1]),
      Phaser.Easing.Linear.None,
      true,
      0,
      -1,
    );
  }

  // ----------------------------------------------------------------------------------------------
  private spawn() {
    this.present()
      .then(() => {
        this.setBumperAnimation();

        if (this._pathFollowerEffect) {
          this._pathFollowerEffect.start();
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  }
}
