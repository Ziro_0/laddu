import Entity from './Entity';
import GameRopeEffect from './effects/GameRopeEffect';
import BallShadowEffect from './effects/BallShadowEffect';

export default class Ball extends Entity {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  basketFront: Phaser.Image;

  private _ballShadowEffect: BallShadowEffect;

  private _isInFrontOfBasket: boolean;

  private _isEasyModeActive: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, entitiesGroup: Phaser.Group) {
    super(game, x, y, 'ball', entitiesGroup);
    this._ballShadowEffect = new BallShadowEffect(this.game, this, entitiesGroup);
  }

  // ----------------------------------------------------------------------------------------------
  def(): void {
    this.setEnabled(false);

    this.physicsBody.setZeroVelocity();
    this.physicsBody.static = true;

    this.loadTexture('ball-deff');
    this.animations.add('anim-deff');
    this.animations.play('anim-deff', 24, false);

    this.events.onAnimationComplete.addOnce(() => {
      this.destroy();
    });
  }

  // ----------------------------------------------------------------------------------------------
  get isEasyModeActive(): boolean {
    return (this._isEasyModeActive);
  }

  // ----------------------------------------------------------------------------------------------
  set isEasyModeActive(value: boolean) {
    if (this._isEasyModeActive === value) {
      return;
    }

    this._isEasyModeActive = value;
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    this.updateAsSolo();
  }

  // ----------------------------------------------------------------------------------------------
  updateShadows(): void {
    this._ballShadowEffect.updateShadows(this.isEasyModeActive);
  }

  // ==============================================================================================
  // protected
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  protected onInitPhysics(): void {
    this.scale.set(0.75);
    this.physicsBody.setCircle(this.width / 2)
    this.physicsBody.collideWorldBounds = true;
    this.physicsBody.fixedRotation = true;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private updateAsSolo(): void {
    this.angle = 0;
    this.updateAsSoloLayering();
    this.updateShadows();
  }

  // ----------------------------------------------------------------------------------------------
  private updateAsSoloLayering(): void {
    const basketFront = this.basketFront;
    if (!basketFront) {
      return;
    }

    const parent = this.parent;
    let childIndex = -1;

    if (this.world.y < basketFront.world.y + basketFront.height / 2) {
      if (this._isInFrontOfBasket) {
        this._isInFrontOfBasket = false;
        childIndex = parent.getChildIndex(basketFront);
      }
    } else {
      if (!this._isInFrontOfBasket) {
        this._isInFrontOfBasket = true;
        childIndex = parent.children.length - 1;
      }
    }

    if (childIndex > -1) {
      parent.setChildIndex(this, childIndex);
    }
  }
}
