export enum CoinIds {
  EASY = 'easy',
  POINTS = 'points',
  SPRINGS = 'springs',
  TIME = 'time',
}

export enum CoinEffects {
  EASY = 'easy',
  POINTS = 'points',
  POINTS_MULTIPLIER = 'pointsMultiplier',
  SPRINGS = 'springs',
  TIME = 'time',
}

// ==============================================================================================--
export function getCoinEffects(id: CoinIds): CoinEffects[] {
  switch (id) {
    case CoinIds.EASY:
      return [CoinEffects.EASY];

    case CoinIds.SPRINGS:
      return [CoinEffects.SPRINGS];

    case CoinIds.TIME:
      return [CoinEffects.TIME];

    case CoinIds.POINTS:
    default:
      return [CoinEffects.POINTS, CoinEffects.POINTS_MULTIPLIER];
  }
}

// ==============================================================================================--
export function getCoinEffectSoundKey(id: CoinIds): string {
  switch (id) {
    case CoinIds.EASY:
      return 'snd-effect-easy';

    case CoinIds.SPRINGS:
      return 'snd-effect-springs';

    case CoinIds.TIME:
      return 'snd-effect-time';

    case CoinIds.POINTS:
      return 'snd-effect-points';

    default:
      return '';
  }
}

// ==============================================================================================--
export function getCoinEffectText(id: CoinIds): string {
  switch (id) {
    case CoinIds.EASY:
      return 'EASY MODE!';

    case CoinIds.SPRINGS:
      return 'SPRINGS!';

    case CoinIds.TIME:
      return 'MORE TIME!';

    case CoinIds.POINTS:
      return 'BONUS POINTS!';

    default:
      return '';
  }
}

// ==============================================================================================--
export function getCoinTexture(id: CoinIds): string {
  switch (id) {
    case CoinIds.EASY:
      return ('coin-blue-black');

    case CoinIds.SPRINGS:
      return ('coin-green-cyan');

    case CoinIds.TIME:
      return ('coin-pink-cyan');

    case CoinIds.POINTS:
    default:
      return ('coin-gold-white');
  }
}
