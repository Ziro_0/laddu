// ================================================================================================

import { getProperty } from '../../../util/util';

/**
 * Specifies game config properties for "color-pin" game.
 */
export default interface IColorPinGameConfig {
  /**
   * If the player scores this number of points or less, present the small awards UI.
   * 
   * Defaults to '3'.
   */
  awardUiPointsSmall?: number;

  /**
   * If the player scores more than `awardUiPointsSmall` but thus number of points or less,
   * present the medium awards UI.
   * Otherwise, iIf the player scores more than this number of points, present the large awards UI.
   * 
   * Defaults to `6`.
   */
  awardUiPointsMedium?: number;

  /**
   * Points awarded for scoring a baskey while a large, moving bumper is active.
   * 
   * Defaults to `20`.
   */
  bumperPointsLargeMoving?: number;

  /**
   * Points awarded for scoring a baskey while a large, non-moving bumper is active.
   * 
   * Defaults to `8`.
   */
  bumperPointsLargeStill?: number;

  /**
   * Points awarded for scoring a baskey while a small, moving bumper is active.
   * 
   * Defaults to `12`.
   */
  bumperPointsSmallMoving?: number;

  /**
   * Points awarded for scoring a baskey while a small, non-moving bumper is active.
   * 
   * Defaults to `3`.
   */
  bumperPointsSmallStill?: number;

   /**
   * Duration, in seconds, that a coin will appear in normal state. Once this time has expired,
   * the coin switches to "warning" mode. See `coinsWarningDurationSecs` for more details.
   * 
   * Defaults to `12` seconds.
   */
  coinsNormalDurationSecs?: number;

  /**
   * Duration, in seconds, that a coin will appear in warning state. While in this state, the
   * coin will blink. Once this time has expired, the coin is removed from the game.
   * 
   * Defaults to `3` seconds.
   */
  coinsWarningDurationSecs?: number;

  /**
   * Should Phaser should draw the P2 physics bodies?
   * 
   * Defaults to `false`.
   */
  debugShowPhysicsBodies?: boolean;

  /**
   * Base points awarded for granting the 'points' effect.
   * 
   * Note that this is influenced by the current bonus points multiplier.
   * 
   * Default is `3`.
   */
  effectPointsAward?: number;

  /**
   * Adds this number of seconds onto the timer when the time-power-up coin is collected.
   * 
   * Defaults to `30`.
   */
  effectTimeAdded?: number;

  /**
   * The gravity used in the P2 physics world.
   * 
   * Defaults to `1500`.
   */
  gravity?: number;

  /**
   * The max number of points awarded if the ball is scored the fastest.
   * 
   * Note that this score won't be possible, due to the time of ball travel, etc, so (if it matters
   * to you) you may want to specify a slight higher max.
   * 
   * See `timeBonusGraceSeconds` for more details on the time bonus.
   * 
   * Note that this is influenced by the current bonus points multiplier.
   * 
   * Defaults to `10`.
   */
  maxTimeBonusPoints?: number;

  /**
   * Bonus points awarded to a score that ahppened while the basket was moving.
   * 
   * Defaults to `5`.
   */
  movingBasketBonusPoints?: number;

  /**
   * Every time the all rebounds off the wall or basket this many times, the player
   * will be awarded rebound points.
   * 
   * @See `reboundPoints`
   * 
   * Defaults to `4`.
   */
  numReboundsPerPoints?: number;

  /**
   * Number of points lost each time a ball is missed.
   * 
   * Note: Do not set negative numbers.
   * 
   * Defaults to `2`.
   */
  pointsLost?: number;

  /**
   * Base points awarded for each ball scored.
   * 
   * The formula for calculating final points for a score is:
   * 
   * `(pointsPerBallScore + time-bonus-points + number-of-rebounds-this-ball-made +
   * bumper-points + wall-points + moving-basket-points) x
   * bonus-points-multipler`
   * 
   * Notes:
   * 1. The bonus-points-multipler is equivalent to the current number of hearts.
   * 
   * 2. See the four `bumperPoints...` properties for details on `bumper-points` mentioned above.
   * 
   * 3. See the `wallBonusPoints` property for details on `wall-points` mentioned above.
   * 
   * 4. See the `movingBasketBonusPoints` property for details on `moving-basket-points`
   * mentioned above.
   * 
   * Defaults to `1` point.
   */
  pointsPerBallScore?: number;

  /**
   * Number of points awarded for each time the ball rebounds, as specified in the
   * `numReboundsPerPoints` property.
   * 
   * Note that this is influenced by the current bonus points multiplier.
   * 
   * Defaults to `1`.
   */
  reboundPoints?: number;

  /**
   * Bounce applied to the ball when colliding with other P2 physics bodies.
   * 
   * Defaults to `0.9`.
   */
  restitution?: number;

  /**
   * Starting duration, in seconds, of one swing of the rope (side to side).
   * 
   * Defaults to `2.0`.
   */
  ropeSwingStartDurationSecs?: number;

  /**
   * Amount of duration, in seconds, that the rope swing changes after each successful
   * wave. If a wave is unsuccessful, the rope swing resets back to the start.
   * 
   * Defaults to `-0.05`.
   */
  ropeSwingDeltaDurationSecs?: number;

  /**
   * The game start time, in seconds.
   * 
   * Defaults to `90`.
   */
  startTimeInSeconds?: number;

  /**
   * The grace time, in seconds, such that if the ball is scored within this amount of time,
   * bonus points are applied. The quicker the ball is scored, the higher the bonus. If it
   * takes the player longer than this time to score, no bonus is applied.
   * 
   * If the ball is scored within this time, the UI will show a "quick bonus"-related display.
   * 
   * See `maxTimeBonusPoints` for the range of the bonus.
   * 
   * For each round of play, the timer starts once the ball is ready on the rope.
   * 
   * Set to `0` to disable the time bonus.
   * 
   * Defaults to `4.0` seconds.
   */
  timeBonusGraceSeconds?: number;

  /**
   * Bonus points awarded to a score, per wall, that was spiked at the time.
   * 
   * Defaults to `2`.
   */
  wallBonusPoints?: number;
}

// ================================================================================================
export function resolveConfig(dataOut?: IColorPinGameConfig): IColorPinGameConfig {
  if (!dataOut) {
    dataOut = {};
  }

  dataOut.awardUiPointsSmall = Math.floor(getProperty(dataOut, 'awardUiPointsSmall', 3));
  dataOut.awardUiPointsMedium = Math.floor(getProperty(dataOut, 'awardUiPointsMedium', 7));

  dataOut.bumperPointsLargeMoving = Math.floor(getProperty(dataOut, 'bumperPointsLargeMoving',
    20));

  dataOut.bumperPointsLargeStill = Math.floor(getProperty(dataOut, 'bumperPointsLargeStill', 8));
  dataOut.bumperPointsSmallMoving = Math.floor(getProperty(dataOut, 'bumperPointsSmallMoving',
    12));

  dataOut.bumperPointsSmallStill = Math.floor(getProperty(dataOut, 'bumperPointsSmallStill', 3));
  dataOut.coinsNormalDurationSecs = getProperty(dataOut, 'coinsNormalDurationSecs', 12);
  dataOut.coinsWarningDurationSecs = getProperty(dataOut, 'coinsWarningDurationSecs', 3);
  dataOut.debugShowPhysicsBodies = getProperty(dataOut, 'debugShowPhysicsBodies', false);
  dataOut.effectPointsAward = Math.floor(getProperty(dataOut, 'effectPointsAward', 3));
  dataOut.effectTimeAdded = Math.floor(getProperty(dataOut, 'effectTimeAdded', 15));
  dataOut.gravity = getProperty(dataOut, 'gravity', 1500);
  dataOut.maxTimeBonusPoints = Math.floor(getProperty(dataOut, 'maxTimeBonusPoints', 10));
  dataOut.movingBasketBonusPoints = Math.floor(getProperty(dataOut, 'movingBasketBonusPoints', 5));
  dataOut.numReboundsPerPoints = Math.floor(getProperty(dataOut, 'numReboundsPerPoints', 4));
  dataOut.pointsLost = Math.abs(Math.floor(getProperty(dataOut, 'pointsLost', 2)));
  dataOut.pointsPerBallScore = Math.floor(getProperty(dataOut, 'pointsPerBallScore', 1));
  dataOut.reboundPoints = Math.floor(getProperty(dataOut, 'reboundPoints', 1));
  dataOut.restitution = getProperty(dataOut, 'restitution', 0.9);
  dataOut.ropeSwingStartDurationSecs = getProperty(dataOut, 'ropeSwingStartDurationSecs', 2.0);
  dataOut.ropeSwingDeltaDurationSecs = getProperty(dataOut, 'ropeSwingDeltaDurationSecs', -0.05);
  dataOut.startTimeInSeconds = getProperty(dataOut, 'startTimeInSeconds', 90);
  dataOut.timeBonusGraceSeconds = getProperty(dataOut, 'timeBonusGraceSeconds', 4.0);
  dataOut.wallBonusPoints = Math.floor(getProperty(dataOut, 'wallBonusPoints', 2));

  return (dataOut);
}
