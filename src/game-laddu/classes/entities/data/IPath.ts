interface IPath {
  /**
   * If `true`, the path is a ellipse. Otherwise, a box path is used.
   */
  isEllipse: boolean;

  /**
   * If `true`, the path travels clockwise. Otherwise, counterclockwise.
   */
  isClockwise: boolean;

  /**
   * If the path is an ellipse, only the first point is used, and it represents the center
   * of the ellipse.
   * 
   * If the path is a box, a series of points, where the following object will travel to all
   * of them in sequence, then repeat starting with the first point.
   */
  points: Phaser.Point[];

  /**
   * If the path is an ellipse, this is half its width (or the full radius if it's a circle).
   */
  ellipseWidth?: number;

  /**
   * If the path is an ellipse, this is half its height (or the full radius if it's a circle).
   */
  ellipseHeight?: number;
}

export default IPath;
