const ROPE_METRICS = [
  {
    length: 24,
    swing: [0, 180],
  },

  {
    length: 21,
    swing: [15, 165],
  },

  {
    length: 18,
    swing: [30, 150],
  },

  {
    length: 15,
    swing: [45, 135],
  },
];

export interface IRopeMetric {
  length: number;
  swing: number;
}

export default function getRandomRopeMetrics(): IRopeMetric {
  const metric = Phaser.ArrayUtils.getRandomItem(ROPE_METRICS);
  return ({
    length: metric.length,
    swing: Phaser.ArrayUtils.getRandomItem(metric.swing),
  });
}
