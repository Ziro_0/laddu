import Entity from './Entity';

export default class Spring extends Entity {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly SPRING_XS = [95, 240, 385];

  private static readonly BUMP_DURATION = 500;

  private _bounceTween: Phaser.Tween;

  private _hits: number;

  private _index: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, group: Phaser.Group, index: number, hits = 1) {
    const x = Spring.SPRING_XS[index];
    super(game, x, 0, 'spring', group, true);
    this.physicsBody.y = this.targetY;
    this._index = index;
    this.setHits(hits);
    this.present();
  }

  // ----------------------------------------------------------------------------------------------
  bump(entityJustContacted: Entity): void {
    this.bounceOtherEntity(entityJustContacted);
    this.bounce();
    this.damageHits();
  }

  // ----------------------------------------------------------------------------------------------
  static Create(game: Phaser.Game, group: Phaser.Group, index: number, hits = 1): Spring {
    return (new Spring(game, group, index, hits));
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    if (this._bounceTween) {
      this._bounceTween.stop();
      this._bounceTween = null;
    }

    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  get index(): number {
    return (this._index);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private bounce(): void {
    this._bounceTween = this.game.tweens.create(this.physicsBody)
      .to(
        {
          y: this.physicsBody.y + 15,
        },
        Spring.BUMP_DURATION / 2,
        Phaser.Easing.Cubic.Out,
        true,
      );

    if (this._hits > 0) {
      this.rebound();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private bounceOtherEntity(entityJustContacted: Entity): void {
    if (!entityJustContacted) {
      return;
    }

    this.game.tweens.create(entityJustContacted.physicsBody)
      .to(
        {
          x: this.game.world.centerX,
          y: -entityJustContacted.height,
        },
        Spring.BUMP_DURATION,
        Phaser.Easing.Linear.None,
        true,
      );
  }

  // ----------------------------------------------------------------------------------------------
  private damageHits(): void {
    const hits = this._hits - 1;
    if (hits <= 0) {
      this.unpresent();
    } else {
      this.setHits(hits);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private present(): void {
    this.alpha = 0;

    this.game.tweens.create(this)
      .to(
        {
          alpha: 1.0,
        },
        Spring.BUMP_DURATION,
        Phaser.Easing.Linear.None,
        true
      )
  }

  // ----------------------------------------------------------------------------------------------
  private rebound(): void {
    this._bounceTween.onComplete.addOnce(() => {
      this._bounceTween = this.game.tweens.create(this.physicsBody)
        .to(
          {
            y: this.targetY,
          },
          Spring.BUMP_DURATION,
          Phaser.Easing.Elastic.Out,
          true,
        );
    });
  }

  // ----------------------------------------------------------------------------------------------
  private setHits(hits: number): void {
    this._hits = Math.floor(Math.max(1, hits));
    this.frame = Math.min(hits - 1, this.animations.frameTotal - 1);
  }

  // ----------------------------------------------------------------------------------------------
  private get targetY(): number {
    return (this.game.height - this.height / 2);
  }

  // ----------------------------------------------------------------------------------------------
  private unpresent(): void {
    this._isPreOrUnpresenting = true;
    this.setEnabled(false);

    this.game.tweens.create(this)
      .to(
        {
          alpha: 0,
        },
        500,
        Phaser.Easing.Linear.None,
        true
      )
      .onComplete.addOnce(() => {
        this.destroy();
      });
  }
}
