import { IPointsResult } from '../../../pages/home/home';
import { Game } from '../../game';
import Listeners from '../../Listeners';
import { getP2PhysicsBodyOf, irandomRange, listenerCallback } from '../../util/util';
import Entity from './Entity';

export default class Basket extends Entity {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly MIN_X = 124;

  private static readonly MAX_X = 420;

  private static readonly LARGE_MIN_X = 164;

  private static readonly LARGE_MAX_X = 410;

  private static readonly MOVE_DURATION_MS = 1500;

  private static readonly MOVE_CONT_DURATION_MS = 5000;

  private static readonly SCALE_DURATION_MS = 750;

  private static readonly BOUNCE_DURATION_MS = 100;

  private static readonly MIN_POINTS_NEEDED_FOR_SLIDING = 100;

  private static readonly MIN_CHANCE_NEEDED_FOR_SLIDING = 0.25;

  private static readonly Y = 690;

  private static readonly LARGE_Y = 719;

  private static readonly ENT_NAME_BOTTOM = 'bottom';

  private static readonly RECT_ENT_NAME_LEFT = 'rectleft';

  private static readonly RECT_ENT_NAME_RIGHT = 'rectright';

  private static readonly CIRC_ENT_NAME_LEFT = 'circleft';

  private static readonly CIRC_ENT_NAME_RIGHT = 'circright';

  /**
   * name, x, y, radius
   */
  private static readonly PHYS_ENTITY_CIRCLES: [string, number, number, number][] = [
    // left
    [Basket.CIRC_ENT_NAME_LEFT, -82, -61, 2.5],

    // right
    [Basket.CIRC_ENT_NAME_RIGHT, 18, -61, 2.5],
  ];

  /**
   * name, x, y, width, height
   */
  private static readonly PHYS_ENTITY_RECTS: [string, number, number, number, number][] = [
    // left
    [Basket.RECT_ENT_NAME_LEFT, -82, -36, 5, 50],

    // right
    [Basket.RECT_ENT_NAME_RIGHT, 18, -36, 5, 50],

    // bottom
    [Basket.ENT_NAME_BOTTOM, -32, -20, 90, 20],
  ];

  private static readonly FRONT_X_OFS = -92;

  private static readonly FRONT_Y_OFS = -71;

  private static readonly NORM_SCALE = 1.25;

  private _shapesNamesByShape: Map<p2.Shape, string>;

  private _basketFront: Phaser.Image;

  private _bounceTween: Phaser.Tween;

  private _bounceLateralTween: Phaser.Tween;

  private _moveContTween: Phaser.Tween;

  private _viewOffset: number;

  private _isLarge: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, viewOffset: number, group: Phaser.Group) {
    super(game, 0, Basket.Y + viewOffset, 'basket', group, true);
    this._viewOffset = viewOffset;
    this.physicsBody.x = this.xRandom();
    this.initFrontImage(group);
  }

  // ----------------------------------------------------------------------------------------------
  bounce(): void {
    if (this._bounceTween) {
      return;
    }

    this.stopMoveContinuous();

    this.bounceTween();
    this.bounceLateralTween();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    this.stopMoveContinuous();
    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  get front(): Phaser.Image {
    return (this._basketFront);
  }

  // ----------------------------------------------------------------------------------------------
  isBottomShape(shape: p2.Shape): boolean {
    return (this._shapesNamesByShape.get(shape) === Basket.ENT_NAME_BOTTOM);
  }

  // ----------------------------------------------------------------------------------------------
  get isLarge(): boolean {
    return (this._isLarge);
  }

  // ----------------------------------------------------------------------------------------------
  set isLarge(value: boolean) {
    if (value === this._isLarge) {
      return;
    }

    this._isLarge = value;

    const START_SCALE = this.scale.x;
    const START_Y = this.physicsBody.y;
    let endScale: number;
    let endY: number;

    if (this._isLarge) {
      endScale = 1.5;
      endY = Basket.LARGE_Y + this._viewOffset;
    } else {
      endScale = Basket.NORM_SCALE;
      endY = Basket.Y + this._viewOffset;
    }

    const target = {
      value: 0,
    };

    const tween = this.game.tweens.create(target)
      .to(
        {
          value: 1.0,
        },
        Basket.SCALE_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
      )

    tween.onUpdateCallback((_tween: Phaser.Tween, _value: number, data: Phaser.TweenData) => {
      const ratio = Phaser.Easing.Bounce.InOut(data.percent);

      const scale = Phaser.Math.linear(START_SCALE, endScale, ratio);
      this.scale.set(scale);
      this._basketFront.scale.set(scale);

      this.physicsBody.y = Phaser.Math.linear(START_Y, endY, ratio);
    });

    tween.onComplete.addOnce(() => {
      this.initPhysicsRectBodies();
    });
  }

  // ----------------------------------------------------------------------------------------------
  get isMovingContinuous(): boolean {
    return (this._moveContTween != null);
  }

  // ----------------------------------------------------------------------------------------------
  move(x?: number): Promise<void> {
    this.stopMoveContinuous();

    if (x === undefined) {
      x = this.xRandom();
    } else {
      x = this.xClamp(x);
    }

    const tween = this.game.tweens.create(this.physicsBody)
      .to(
        {
          x,
        },
        Basket.MOVE_DURATION_MS,
        Phaser.Easing.Elastic.InOut,
        true,
      );

    return new Promise((resolve) => {
      tween.onComplete.addOnce(() => {
        resolve();
      });
    });
  }

  // ----------------------------------------------------------------------------------------------
  nextWave(wasPrevWaveSuccessful?: boolean): void {
    if (wasPrevWaveSuccessful === undefined) {
      return;
    }

    if (!this.moveContinuousChance()) {
      this.moveChance();
    }
  }

  // ----------------------------------------------------------------------------------------------
  get physicsBody(): Phaser.Physics.P2.Body {
    return (getP2PhysicsBodyOf(this));
  }

  // ----------------------------------------------------------------------------------------------
  postUpdate(): void {
    super.postUpdate();

    const twn = this._bounceLateralTween;
    if (twn) {
      const BOUNCE_X_OFS = 5;
      const data: Phaser.TweenData = twn.timeline[twn.current];
      this.x = this.x + BOUNCE_X_OFS * data.percent;
    }
  }

  // ----------------------------------------------------------------------------------------------
  setNormScale(): void {
    this._isLarge = false;
    this.scale.set(Basket.NORM_SCALE);
    this._basketFront.scale.set(Basket.NORM_SCALE);
    this.physicsBody.y = Basket.Y + this._viewOffset;
    this.initPhysicsRectBodies();
  }

  // ----------------------------------------------------------------------------------------------
  stopMoveContinuous(): void {
    if (this._moveContTween) {
      this._moveContTween.stop();
      this._moveContTween = null;
    }
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    this.updateBasketFrontPosition();
  }

  // ==============================================================================================
  // protected
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  protected onInitPhysics(): void {
    this.initPhysicsRectBodies();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private bounceLateralTween(): void {
    const target = {
      value: 0,
    };

    this._bounceLateralTween = this.game.tweens.create(target)
      .to(
        {
          value: 1.0,
        },
        Basket.BOUNCE_DURATION_MS,
        Phaser.Easing.Elastic.InOut,
        true,
      )
      .yoyo(true)

    this._bounceLateralTween.onComplete.addOnce(() => {
      this._bounceLateralTween = null;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private bounceTween(): void {
    const BOUNCE_Y_OFS = 10;
    const basketBody = this.physicsBody;
    this._bounceTween = this.game.tweens.create(basketBody)
      .to(
        {
          y: basketBody.y + BOUNCE_Y_OFS,
        },
        Basket.BOUNCE_DURATION_MS,
        Phaser.Easing.Cubic.Out,
        true)
      .yoyo(true);

    this._bounceTween.onComplete.addOnce(() => {
      this._bounceTween = null;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private getPlayerScore(): number {
    const points: IPointsResult = {};
    listenerCallback(<Game> this.game, Listeners.GET_POINTS, points);
    return (points.value);
  }

  // ----------------------------------------------------------------------------------------------
  private initCircEntity(name: string, x: number, y: number, radius: number): void {
    const scale = this.scale.x;
    const shape = this.physicsBody.addCircle(radius * scale, x * scale, y * scale);
    this._shapesNamesByShape.set(shape, name);
  }

  // ----------------------------------------------------------------------------------------------
  private initPhysicsRectBodies(): void {
    this._shapesNamesByShape = new Map();

    this.physicsBody.clearShapes();

    Basket.PHYS_ENTITY_RECTS.forEach((rect) => {
      const [name, x, y, width, height] = rect;
      this.initRectEntity(name, x, y, width, height);
    });

    Basket.PHYS_ENTITY_CIRCLES.forEach((rect) => {
      const [name, x, y, radius] = rect;
      this.initCircEntity(name, x, y, radius);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private initRectEntity(name: string, x: number, y: number, width: number, height: number): void {
    const scale = this.scale.x;
    const shape = this.physicsBody.addRectangle(width * scale, height * scale,
      x * scale, y * scale);
    this._shapesNamesByShape.set(shape, name);
  }

  // ----------------------------------------------------------------------------------------------
  private initFrontImage(group: Phaser.Group): void {
    this._basketFront = this.game.add.image(0, 0, 'basket-front', undefined, group);
    this.updateBasketFrontPosition();
  }

  // ----------------------------------------------------------------------------------------------
  private moveChance(): Promise<void> {
    if (Math.random() >= 0.5) {
      return (this.move());
    }

    return Promise.resolve();
  }

  // ----------------------------------------------------------------------------------------------
  private moveContinuous(): void {
    const xMin = this.xMin();
    const xMax = this.xMax();
    const xStart = Phaser.Utils.randomChoice(xMin, xMax);

    this.move(xStart)
      .then(() => {
        this._moveContTween = this.game.tweens.create(this.physicsBody)
          .to(
            {
              x: xStart === xMin ? xMax : xMin,
            },
            Basket.MOVE_CONT_DURATION_MS,
            Phaser.Easing.Cubic.InOut,
            true,
            0,
            -1,
            true,
          );
      })
  }

  // ----------------------------------------------------------------------------------------------
  private moveContinuousChance(): boolean {
    if (this.getPlayerScore() < Basket.MIN_POINTS_NEEDED_FOR_SLIDING) {
      return (false);
    }

    if (Math.random() >= Basket.MIN_CHANCE_NEEDED_FOR_SLIDING) {
      return (false);
    }

    this.moveContinuous();
    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  private updateBasketFrontPosition(): void {
    this._basketFront.position.set(
      this.x + Basket.FRONT_X_OFS * this.scale.x,
      this.y + Basket.FRONT_Y_OFS * this.scale.y,
    );
  }

  // ----------------------------------------------------------------------------------------------
  private xClamp(x: number): number {
    return (Phaser.Math.clamp(x, this.xMin(), this.xMax()));
  }

  // ----------------------------------------------------------------------------------------------
  private xMax(): number {
    return (this._isLarge ? Basket.LARGE_MAX_X : Basket.MAX_X);
  }

  // ----------------------------------------------------------------------------------------------
  private xMin(): number {
    return (this._isLarge ? Basket.LARGE_MIN_X : Basket.MIN_X);
  }

  // ----------------------------------------------------------------------------------------------
  private xRandom(): number {
    return (irandomRange(this.xMin(), this.xMax()));
  }
}
