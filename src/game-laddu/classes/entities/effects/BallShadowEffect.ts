import Ball from '../Ball';

export default class BallShadowEffect extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _ball: Ball;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, ball: Ball, group: Phaser.Group) {
    super(game, group);
    this._ball = ball;
    this.game.add.existing(this);
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    this.updateImages();
    super.update();
  }

  // ----------------------------------------------------------------------------------------------
  updateShadows(shouldProduceShadows: boolean): void {
    if (shouldProduceShadows) {
      this.createShadowImage();
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createShadowImage(): void {
    const shadowImage = this.game.add.image(
      this._ball.world.x,
      this._ball.world.y,
      'ball',
      undefined,
      this,
    );

    shadowImage.alpha = 0.5;
    shadowImage.anchor.set(0.5);

    this.game.tweens.create(shadowImage.scale)
      .to(
        {
          x: 0,
          y: 0,
        },
        500,
        Phaser.Easing.Linear.None,
        true,
      ).onComplete.addOnce(() => {
        shadowImage.destroy();
      });
  }

  // ----------------------------------------------------------------------------------------------
  private updateImages(): void {
    this.forEach((image: Phaser.Image) => {
      if (Math.random() >= 0.5) {
        image.visible = !image.visible;
      }
    });
  }
}
