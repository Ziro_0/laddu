import Entity from '../Entity';

// ================================================================================================
interface IFloatingTarget {
  value: number;
}

export default class FloatingEffect  {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly FLOATING_DURATION_MS = 500;

  private static readonly FLOATING_OFFSET = 4;

  private _floatingOffsets: number[];

  private _floatingTarget: IFloatingTarget;

  private _floatingTween: Phaser.Tween;

  private _entity: Entity;

  private _isRunning: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(entity: Entity) {
    this._entity = entity;
  }

  // ----------------------------------------------------------------------------------------------
  get isRunning(): boolean {
    return (this._isRunning);
  }

  // ----------------------------------------------------------------------------------------------
  start(): void {
    if (this._isRunning) {
      return;
    }

    this._isRunning = true;

    const FLOATING_OFFSET = FloatingEffect.FLOATING_OFFSET;
    this._floatingOffsets = [-FLOATING_OFFSET, FLOATING_OFFSET];

    this.createInitialFloatingTween()
      .then(() => {
        this.createMainFloatingTween();
      })
      .catch((error) => {
        console.warn(error);
      });
  }

  // ----------------------------------------------------------------------------------------------
  stop(): void {
    if (this._floatingTween) {
      this._floatingTween.stop();
      this._floatingTween = null;
    }

    this._isRunning = false;
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (!this._isRunning) {
      return;
    }

    if (this._entity) {
      this._entity.y = this._entity.physicsBody.y + this._floatingTarget.value;
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createInitialFloatingTween(): Promise<void> {
    this._floatingTarget = {
      value: 0,
    };

    this._floatingTween = this._entity.game.tweens.create(this._floatingTarget)
      .to(
        {
          value: this._floatingOffsets[0],
        },
        FloatingEffect.FLOATING_DURATION_MS / 2,
        Phaser.Easing.Cubic.InOut,
        true,
      )

    return new Promise((resolve) => {
      this._floatingTween.onComplete.addOnce(() => resolve());
    });
  }

  // ----------------------------------------------------------------------------------------------
  private createMainFloatingTween(): void {
    Phaser.ArrayUtils.rotateLeft(this._floatingOffsets);

    this._floatingTween = this._entity.game.tweens.create(this._floatingTarget)
      .to(
        {
          value: this._floatingOffsets[0],
        },
        FloatingEffect.FLOATING_DURATION_MS,
        Phaser.Easing.Cubic.InOut,
        true,
        0, -1, true,
      );
  }
}
