export default class ImpactEffect extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static COLORS = [
    0xffffff,
    0xffff00,
    0xff00ff,
    0x00ffff,
  ];

  private static TWEEN_DURATION_MS = 250;

  private _startScale: number;

  private _endScale: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, group: Phaser.Group, startScale: number,
    endScale: number) {
    super(game, group);

    this.game.add.existing(this);

    this.x = x;
    this.y = y;

    this.initMainTween();

    this._startScale = startScale;
    this._endScale = endScale;

    this.initRing();
    this.initStar();
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    this.forEach((image: Phaser.Image) => {
      image.tint = Phaser.ArrayUtils.getRandomItem(ImpactEffect.COLORS);
    });
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private initMainTween(): void {
    this.game.tweens.create(this)
      .to(
        {
          alpha: 0,
          angle: 180,
        },
        ImpactEffect.TWEEN_DURATION_MS,
        Phaser.Easing.Cubic.In,
        true
      )
      .onComplete.addOnce(() => {
        this.destroy();
      });
  }

  // ----------------------------------------------------------------------------------------------
  private initRing(): void {
    const ring = this.game.add.image(0, 0, 'cut-rope-effect-ring', undefined, this);
    ring.anchor.set(0.5);
    this.initScaleTween(ring);
  }

  // ----------------------------------------------------------------------------------------------
  private initScaleTween(target: Phaser.Image): void {
    target.scale.set(this._startScale);

    this.game.tweens.create(target.scale)
      .to(
        {
          x: this._endScale,
          y: this._endScale,
        },
        ImpactEffect.TWEEN_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
      );
  }

  // ----------------------------------------------------------------------------------------------
  private initStar(): void {
    const star = this.game.add.image(0, 0, 'cut-rope-effect-star', undefined, this);
    star.anchor.set(0.5);
    this.initScaleTween(star);
  }

}
