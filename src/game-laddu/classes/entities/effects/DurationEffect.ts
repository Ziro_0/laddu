import Entity from '../Entity';

export default class DurationEffect {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _entity: Entity;

  private _timer: Phaser.Timer;

  private _onComplete: Phaser.Signal;

  private _normalDurationMs: number;

  private _warningDurationMs: number;

  private _isRunning: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(entity: Entity, normalDurationMs: number, warningDurationMs: number) {
    this._entity = entity;

    this._onComplete = new Phaser.Signal();

    this._normalDurationMs = normalDurationMs;
    this._warningDurationMs = warningDurationMs;
  }

  // ----------------------------------------------------------------------------------------------
  get isRunning(): boolean {
    return (this._isRunning);
  }

  // ----------------------------------------------------------------------------------------------
  get onComplete(): Phaser.Signal {
    return (this._onComplete);
  }

  // ----------------------------------------------------------------------------------------------
  start(): void {
    if (!this._entity) {
      console.warn('no entity assigned during construction');
      return;
    }

    if (this._isRunning) {
      return;
    }

    this._isRunning = true;

    this._timer = this._entity.game.time.create(false);
    this._timer.start();

    this._timer.add(this._normalDurationMs, () => {
      this.warn();
    });
  }

  // ----------------------------------------------------------------------------------------------
  stop(): void {
    this._isRunning = false;

    this._onComplete.removeAll();

    if (this._timer) {
      this._timer.destroy();
      this._timer = null;
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private warn(): void {
    this._timer.loop(50, () => {
      this._entity.visible = !this._entity.visible;
    });

    this._timer.add(this._warningDurationMs, () => {
      this._timer.removeAll();
      this._entity.visible = true;
      this._onComplete.dispatch(this._entity, this);
    });
  }
}
