import { removeFromArray } from '../../../util/util';
import Ball from '../Ball';
import RopeSegmentEffect from './RopeSegmentEffect';

export default class GameRopeEffect extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly Y = 63;

  private static readonly MAX_NUM_MOST_RECENT_BALL_POSITIONS = 10;

  private static readonly GROW_RATE_MS = 20;

  private _segments: RopeSegmentEffect[];

  private _freeSegments: RopeSegmentEffect[];

  private _mostRecentBallPositions: Phaser.Point[];

  private _freePoints: Phaser.Point[];

  private _viewOffset: number;

  private _ball: Ball;

  private _ropeSegmentSize: Phaser.Point;

  private _swingingTween: Phaser.Tween;

  private _timer: Phaser.Timer;

  private _newBallTimerEvent: Phaser.TimerEvent;

  private _swingDurationMs: number;

  private _isReadyToFire: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, viewOffset: number, timer: Phaser.Timer, group: Phaser.Group) {
    super(game, group);

    this._viewOffset = viewOffset;
    this._timer = timer;

    this._mostRecentBallPositions = [];
    this._segments = [];
    this._freeSegments = [];
    this._freePoints = [];

    this.x = this.game.world.centerX;
    this.y = GameRopeEffect.Y;
  }

  // ----------------------------------------------------------------------------------------------
  cut(): void {
    this.stopSwing();

    let index = this._segments.length - 1;
    while (index >= 0) {
      this.cutSegment(this._segments[index]);
      index -= 1;
    };
  }

  // ----------------------------------------------------------------------------------------------
  fire(): Ball {
    if (!this.isReadyToFire) {
      return (null);
    }

    this._isReadyToFire = false;

    const ball = this.detachBall();
    if (!ball) {
      return (null);
    }

    this.applyVelocityToFiredBall(ball);

    return (ball);
  }

  // ----------------------------------------------------------------------------------------------
  getSegmentPositionOf(index: number): Phaser.Point {
    const segment = this._segments[index];
    if (!segment) {
      return (null);
    }

    return (new Phaser.Point(segment.world.x, segment.world.y));
  }

  // ----------------------------------------------------------------------------------------------
  get isReadyToFire(): boolean {
    return (this._isReadyToFire);
  }

  // ----------------------------------------------------------------------------------------------
  newBall(ropeLength: number, swingAngle: number, isEasyModeActive: boolean): Promise<boolean> {
    return new Promise((resolve) => {
      if (this._ball) {
        resolve(false);
      }

      this._isReadyToFire = false;

      this.angle = swingAngle;

      this.ropeLength = 1;

      if (this._newBallTimerEvent) {
        this._timer.remove(this._newBallTimerEvent);
      };

      const ball = new Ball(this.game, 0, 0, <Phaser.Group> this.parent);
      ball.isEasyModeActive = isEasyModeActive;
      this.attachBall(ball);

      this._newBallTimerEvent = this._timer.repeat(
        GameRopeEffect.GROW_RATE_MS,
        ropeLength - 1,
        () => {
          this.ropeLength += 1;
          if (this.ropeLength === ropeLength) {
            this._isReadyToFire = true;
            this.swing(swingAngle);
            resolve(true);
          }
        }
      );
    });
  }

  // ----------------------------------------------------------------------------------------------
  get ropeLength(): number {
    return (this._segments.length);
  }

  // ----------------------------------------------------------------------------------------------
  set ropeLength(value: number) {
    if (value <= 0) {
      return;
    }

    const lengthDiff = value - this.ropeLength;
    if (lengthDiff === 0) {
      return;
    }

    if (lengthDiff > 0) {
      for (let index = 0; index < lengthDiff; index += 1) {
        this.addSegment();
      }
    } else {
      for (let index = lengthDiff; index < 0; index -= 1) {
        this.removeLastSegment();
      }
    }

    if (this._ball) {
      this._ball.physicsBody.x = this.ropeSegmentSize.x * value;
    }
  }

  // ----------------------------------------------------------------------------------------------
  swing(swingAngle: number): void {
    this.stopSwing();

    const oppositeAngle = 180 - swingAngle;
    this._swingingTween = this.game.tweens.create(this)
      .to(
        {
          angle: oppositeAngle,
        },
        this.swingDurationMs,
        Phaser.Easing.Quadratic.InOut,
        true,
        0,
        -1,
        true,
      );
  }

  // ----------------------------------------------------------------------------------------------
  get swingDurationMs(): number {
    return (this._swingDurationMs);
  }

  // ----------------------------------------------------------------------------------------------
  set swingDurationMs(value: number) {
    this._swingDurationMs = value;
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    this.updateBall();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private addSegment(): void {
    let ropeSegment = this._freeSegments.pop();
    if (!ropeSegment) {
      ropeSegment = this.newRopeSegment();
    }

    ropeSegment.anchor.set(0, 0.5);
    ropeSegment.angle = 0;
    ropeSegment.x = this._segments.length * this.ropeSegmentSize.x;
    ropeSegment.y = 0;
    ropeSegment.visible = true;

    this.addAt(ropeSegment, 0);
    this._segments.push(ropeSegment);
  }

  // ----------------------------------------------------------------------------------------------
  private applyVelocityToFiredBall(ball: Ball): void {
    const firingAngle = GameRopeEffect.CalcFiringAngle(ball);
    const velofityMagnitude = this.calcVelocityMagnitude();
    const xv = Math.cos(Phaser.Math.degToRad(firingAngle)) * velofityMagnitude;
    const yv = -Math.sin(Phaser.Math.degToRad(firingAngle)) * velofityMagnitude;

    ball.physicsBody.velocity.x = xv;
    ball.physicsBody.velocity.y = yv;
  }

  // ----------------------------------------------------------------------------------------------
  private attachBall(ball: Ball): void {
    if (this._ball) {
      return;
    }

    this._mostRecentBallPositions.length = 0;

    this._ball = ball;

    ball.physicsBody.x = this.width;
    ball.physicsBody.y = 0;
    ball.physicsBody.static = true;

    this.add(ball);
  }

  // ----------------------------------------------------------------------------------------------
  private static CalcFiringAngle(ball: Ball): number {
    return (Phaser.Math.wrapValue(
      270 + Phaser.Math.radToDeg(
        Phaser.Math.angleBetweenY(
          ball.previousPosition.x,
          ball.previousPosition.y,
          ball.world.x,
          ball.world.y,
        )
      ),
      0,
      360
    ));
  }

  // ----------------------------------------------------------------------------------------------
  private calcVelocityMagnitude(): number {
    const distances: number[] = [];
    for (let index = this._mostRecentBallPositions.length - 1; index > 0; index -= 1) {
      const currPoint = this._mostRecentBallPositions[index];
      const prevPoint = this._mostRecentBallPositions[index - 1];
      distances.push(Phaser.Math.distance(
        currPoint.x, currPoint.y,
        prevPoint.x, prevPoint.y,
      ));
    }

    return (Phaser.Math.average(...distances) * 75);
  }

  // ----------------------------------------------------------------------------------------------
  private cutSegment(segment: RopeSegmentEffect): void {
    removeFromArray(this._segments, segment);

    segment.cut()
      .then((rs) => {
        this._freeSegments.push(rs);
      });
  }

  // ----------------------------------------------------------------------------------------------
  private detachBall(): Ball {
    const ball = this._ball;
    if (!ball) {
      return (null);
    }

    this._ball = null;

    ball.physicsBody.x = ball.world.x;
    ball.physicsBody.y = ball.world.y;
    ball.physicsBody.dynamic = true;

    this.remove(ball);
    this.game.add.existing(ball);

    return (ball);
  }

  // ----------------------------------------------------------------------------------------------
  private newRopeSegment(): RopeSegmentEffect {
    return (new RopeSegmentEffect(this.game, this._viewOffset, <Phaser.Group> this.parent));
  }

  // ----------------------------------------------------------------------------------------------
  private removeLastSegment(): void {
    const segment = this._segments.pop();
    this.remove(segment);
    this._freeSegments.push(segment);
  }

  // ----------------------------------------------------------------------------------------------
  private get ropeSegmentSize(): Phaser.Point {
    if (this._ropeSegmentSize) {
      return (this._ropeSegmentSize);
    }

    let segment = this._segments[0];
    if (!segment) {
      segment = this.newRopeSegment();
      this._freeSegments.push(segment);
    }

    this._ropeSegmentSize = new Phaser.Point(segment.width, segment.height);
    return (this._ropeSegmentSize);
  }

  // ----------------------------------------------------------------------------------------------
  private stopSwing(): void {
    if (this._swingingTween) {
      this._swingingTween.stop();
      this._swingingTween = null;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private updateBall(): void {
    if (!this._ball) {
      return;
    }

    this._ball.angle = -this.angle;
    this._ball.updateShadows();

    if (this.isReadyToFire) {
      this.updateBallMostRecentPosition();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private updateBallMostRecentPosition(): void {
    let point = this._freePoints.pop() || new Phaser.Point();
    point.set(this._ball.world.x, this._ball.world.y);

    this._mostRecentBallPositions.push(point);

    while (this._mostRecentBallPositions.length > GameRopeEffect.MAX_NUM_MOST_RECENT_BALL_POSITIONS) {
      point = this._mostRecentBallPositions.shift();
      this._freePoints.push(point);
    }
  }
}
