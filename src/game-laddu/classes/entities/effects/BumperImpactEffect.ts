import Bumper from '../Bumper';

export default class BumperImpactEffect extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static COLORS = [
    0xffffff,
    0xffff00,
    0xff00ff,
    0x00ffff,
  ];

  private static TWEEN_DURATION_MS = 500;

  private _bumper: Bumper;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, bumper: Bumper) {
    super(game, bumper.x, bumper.y, 'bumper-impact');
    this.game.add.existing(this);

    this.anchor.set(0.5);

    this._bumper = bumper;
    this.scale.set(this._bumper.scale.x);
    this._bumper.parent.addChild(this);

    this.initTween();
  }

  // ----------------------------------------------------------------------------------------------
  static Create(game: Phaser.Game, bumper: Bumper): BumperImpactEffect {
    return (new BumperImpactEffect(game, bumper));
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    this.x = this._bumper.x;
    this.y = this._bumper.y;
    this.tint = Phaser.ArrayUtils.getRandomItem(BumperImpactEffect.COLORS);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private initTween(): void {
    const target = {
      value: 0,
    };

    const tween = this.game.tweens.create(target)
      .to(
        {
          value: 1.0,
        },
        BumperImpactEffect.TWEEN_DURATION_MS,
        Phaser.Easing.Cubic.Out,
        true,
      );

    const START_SCALE = this._bumper.scale.x;
    const TARGET_SCALE = START_SCALE * 1.5;
    tween.onUpdateCallback((_tween: Phaser.Tween, value: number) => {
      this.scale.set(Phaser.Math.linear(START_SCALE, TARGET_SCALE, value));
      this.alpha = 1.0 - value;
    });

    tween.onComplete.addOnce(() => {
      this.destroy();
    });
  }
}
