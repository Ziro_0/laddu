import { irandom, irandomRange } from '../../../util/util';
import IPath from '../data/IPath';
import Entity from '../Entity';

export default class PathFollowerEffect {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly MOVE_TO_DURATION_BOX_MS = [1500, 3000];

  private static readonly MOVE_TO_DURATION_ELLIPSE_MS = [3000, 7000];

  private _path: IPath;

  private _entity: Entity;

  private _moveTween: Phaser.Tween;

  private _ellipsePoint: Phaser.Point;

  private _boxPointIndex: number;

  private _epllipseStartRadians: number;

  private _isRunning: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(entity: Entity, path: IPath) {
    this._entity = entity;
    this._path = path;

    this._ellipsePoint = new Phaser.Point();

    this.setRandomPoint();
  }

  // ----------------------------------------------------------------------------------------------
  get isRunning(): boolean {
    return (this._isRunning);
  }

  // ----------------------------------------------------------------------------------------------
  start(): void {
    if (!this._entity) {
      console.warn('no entity assigned during construction');
      return;
    }

    if (this._isRunning) {
      return;
    }

    this._entity.events.onDestroy.addOnce(this.onEntityDestroy, this);

    this._isRunning = true;

    if (this._path.isEllipse) {
      this.moveAsCircle();
    } else {
      this.moveToNextPoint();
    }
  }

  // ----------------------------------------------------------------------------------------------
  stop(): void {
    this._isRunning = false;

    if (this._entity) {
      this._entity.events.onDestroy.remove(this.onEntityDestroy, this);
    }

    if (this._moveTween) {
      this._moveTween.stop();
      this._moveTween = null;
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private getNextPoint(): Phaser.Point {
    const points = this._path.points;
    this._boxPointIndex = (this._boxPointIndex + 1) % points.length;
    return (points[this._boxPointIndex]);
  }

  // ----------------------------------------------------------------------------------------------
  private moveAsCircle(): void {
    const target = {
      value: 0,
    };

    this._moveTween = this._entity.game.tweens.create(target)
      .to(
        {
          value: 1.0,
        },
        irandomRange(
          PathFollowerEffect.MOVE_TO_DURATION_ELLIPSE_MS[0],
          PathFollowerEffect.MOVE_TO_DURATION_ELLIPSE_MS[1],
        ),
        Phaser.Easing.Linear.None,
        true,
        0,
        -1,
      );

    this._moveTween.reverse = !this._path.isClockwise;

    this._moveTween.onUpdateCallback((_tween: Phaser.Tween, value: number) => {
      const TWO_PI = 2 * Math.PI;
      const radians = this._epllipseStartRadians + (value * TWO_PI);
      this.updateEllipsePoint(radians);
      this._entity.physicsBody.x = this._ellipsePoint.x;
      this._entity.physicsBody.y = this._ellipsePoint.y;
    });
  }

  // ----------------------------------------------------------------------------------------------
  private moveToNextPoint(): void {
    const point = this.getNextPoint();
    if (!point) {
      return;
    }

    this._moveTween = this._entity.game.tweens.create(this._entity.physicsBody)
      .to(
        {
          x: point.x,
          y: point.y,
        },
        irandomRange(
          PathFollowerEffect.MOVE_TO_DURATION_BOX_MS[0],
          PathFollowerEffect.MOVE_TO_DURATION_BOX_MS[1],
        ),
        Phaser.Easing.Cubic.InOut,
        true,
      )

    this._moveTween.onComplete.addOnce(() => this.moveToNextPoint());
  }

  // ----------------------------------------------------------------------------------------------
  private onEntityDestroy(): void {
    this.stop();
  }

  // ----------------------------------------------------------------------------------------------
  private setRandomPoint(): void {
    let point: Phaser.Point;

    if (this._path.isEllipse) {
      point = this.setRandomPointEllipse();
    } else {
      point = this.setRandomPointBox();
    }

    this._entity.physicsBody.x = point.x;
    this._entity.physicsBody.y = point.y;
  }

  // ----------------------------------------------------------------------------------------------
  private setRandomPointBox(): Phaser.Point {
    this._boxPointIndex = irandom(this._path.points.length - 1);
    return (this._path.points[this._boxPointIndex]);
  }

  // ----------------------------------------------------------------------------------------------
  private setRandomPointEllipse(): Phaser.Point {
    this._epllipseStartRadians = irandom(2 * Math.PI);
    this.updateEllipsePoint(this._epllipseStartRadians);
    return (this._ellipsePoint);
  }

  // ----------------------------------------------------------------------------------------------
  private updateEllipsePoint(radians: number): void {
    const point = this._path.points[0];
    this._ellipsePoint.set(
      point.x + this._path.ellipseWidth * Math.cos(radians),
      point.y + this._path.ellipseHeight * Math.sin(radians),
    );
  }
}
