export default class RopeCutterEffect extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly TWEEN_DURATION_MS = 200;

  private static readonly Y = 40;

  private _motionTween: Phaser.Tween;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, group: Phaser.Group) {
    super(game, 0, 0, 'rope-cutter');
    this.anchor.set(0, 0.5);
    group.add(this);
    this.visible = false;
  }

  // ----------------------------------------------------------------------------------------------
  start(): Promise<void> {
    const isLeftToRight = Math.random() >= 0.5;
    let xTarget: number;

    if (isLeftToRight) {
      this.rotation = Math.PI;
      this.x = -this.width;
      xTarget = this.game.width + this.width;
    } else {
      this.x = this.game.width;
      xTarget = -this.width;
    }

    this.visible = true;

    return (new Promise((resolve) => {

      this._motionTween = this.game.tweens.create(this)
        .to(
          {
            x: xTarget,
          },
          RopeCutterEffect.TWEEN_DURATION_MS,
          Phaser.Easing.Linear.None,
          true
        );

      this._motionTween.onUpdateCallback(() => {
        let hasCrossdHalfWay: boolean;
        if (isLeftToRight) {
          hasCrossdHalfWay = this.x >= this.game.world.centerX;
        } else {
          hasCrossdHalfWay = this.x <= this.game.world.centerX;
        }

        if (hasCrossdHalfWay) {
          this._motionTween.onUpdateCallback(null);
          resolve();
        }
      });

      this._motionTween.onComplete.addOnce(() => {
        this._motionTween = null;
        this.visible = false;
      });
    }));
  }
}
