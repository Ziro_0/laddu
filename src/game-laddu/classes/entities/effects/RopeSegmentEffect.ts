import { irandomRange } from '../../../util/util';

export default class RopeSegmentEffect extends Phaser.Image {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _viewOffset: number;

  private _isCut: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, viewOffset: number, group: Phaser.Group) {
    super(game, 0, 0, 'rope-segment');
    this._viewOffset = viewOffset;
    group.add(this);
  }

  // ----------------------------------------------------------------------------------------------
  cut(): Promise<this> {
    if (this._isCut) {
      return;
    }

    this._isCut = true;

    return new Promise((resolve) =>
      {
        this.anchor.set(0.5);
        this.x = this.world.x;
        this.y = this.world.y;

        this.parent.removeChild(this);
        this.game.add.existing(this);

        const X = 80;
        const Y = 150 + this._viewOffset;
        const WIDTH = 320;
        const HEIGHT = 150;
        const DURATION_MS = irandomRange(1000, 1500);

        this.game.tweens.create(this)
          .to(
            {
              x: irandomRange(X, X + WIDTH),
            },
            DURATION_MS,
            Phaser.Easing.Linear.None,
            true,
          )

          .onComplete.addOnce(() => {
            this._isCut = false;
            this.visible = false;
            resolve(this);
          });

        this.game.tweens.create(this)
          .to(
            {
              y: irandomRange(Y, Y + HEIGHT),
            },
            DURATION_MS,
            Phaser.Easing.Cubic.In,
            true,
          )
      }
    );
  }

  // ----------------------------------------------------------------------------------------------
  update(): void {
    if (this._isCut) {
      if (Math.random() >= 0.75) {
        this.visible = !this.visible;
        this.angle = irandomRange(-180, 180);
      }
    }
  }
}
