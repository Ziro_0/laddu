import { Game } from '../../../game';

export default class CameraSpringEffect {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly INTENSITY = 15;

  private static readonly MAIN_DURATION_MS = 500;

  private static readonly EFFECT_DURATION_MS = 100;

  private _game: Game;

  private _tween: Phaser.Tween;

  private _origBounds: Phaser.Rectangle;

  private _value: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    this._game = game;
  }

  // ----------------------------------------------------------------------------------------------
  start(): void {
    if (this._tween) {
      return;
    }

    this._origBounds = this._game.world.bounds.clone();

    // allow some space for spring effects
    this._game.world.setBounds(
      this._origBounds.x,
      -CameraSpringEffect.INTENSITY,
      this._origBounds.width,
      this._game.height + CameraSpringEffect.INTENSITY * 2,
    );

    this._value = 0;

    this._tween = this._game.tweens.create(this)
      .to(
        {
          _value: 2 * Math.PI,
        },
        CameraSpringEffect.EFFECT_DURATION_MS,
        Phaser.Easing.Linear.name,
        true,
        0,
        -1,
      );

    this._tween.onUpdateCallback((_tween: Phaser.Tween, _value: number,
      data: Phaser.TweenData) => {
      const ratio = 1.0 - data.percent;
      this._game.camera.y = Math.sin(this._value) * CameraSpringEffect.INTENSITY * ratio;
    });

    this._tween.onComplete.addOnce(() => {
      this._tween = null;
      this._game.camera.y = 0;
    });

    const timer = this._game.time.create();
    timer.start();
    timer.add(CameraSpringEffect.MAIN_DURATION_MS, () => {
      this.stop();
    });
  }

  // ----------------------------------------------------------------------------------------------
  stop(): void {
    if (this._tween) {
      this._tween.stop();
      this._tween = null;
    }

    this._game.world.setBounds(
      this._origBounds.x,
      this._origBounds.y,
      this._origBounds.width,
      this._origBounds.height,
    );

    this._game.camera.y = 0;
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
}
