// ================================================================================================
interface ITweenTarget {
  value: number;
}

export default class StarPanel extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly PRESENT_TWEENS_DURATION_MS = 200;

  private static readonly UNPRESENT_TWEENS_DURATION_MS = 400;

  private static readonly SHINE_TWEENS_DURATION_MS = 400;

  private _text: Phaser.Text;

  private _xOrig: number;

  private _yOrig: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, group: Phaser.Group) {
    super(game, group);

    this.x = this.game.world.centerX;
    this.y = this.game.world.centerY - this.game.height * 0.1;

    this._xOrig = this.x;
    this._yOrig = this.y;

    const panel = this.game.add.image(0, 0, 'star-panel', undefined, this);
    panel.anchor.set(0.5);

    this._text = this.game.add.text(0, 0, '99', {
      font: 'sunspire',
      fontSize: 80,
      fill: '#333333',
    }, this);

    this._text.anchor.set(0.5);

    this.visible = false;
  }

  // ----------------------------------------------------------------------------------------------
  present(text: number | string, scale: number, shouldShine = false): void {
    this.setStartingState(text, scale);

    this.presentTween(scale)
      .then(() => this.shineTween(shouldShine))
      .then(() => this.unpresentTween())
      .catch((error) => console.warn(error));
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createShine(): Phaser.Graphics {
    const shine = this.game.add.graphics(-300, 0, this);
    shine.beginFill(0xffffff, 1.0);

    // draws a rectangle that's taller than the star (so the shine effect, while rotated, still
    // covers the entire star)
    shine.drawRect(-42, -250, 84, 500);

    shine.endFill();
    shine.rotation = Math.PI / 4;
    shine.mask = this.createStarMask();
    return (shine);
  }

  // ----------------------------------------------------------------------------------------------
  private createStarMask(): Phaser.Graphics {
    const mask = this.game.add.graphics(0, 0, this);
    mask.beginFill();

    // draws a polygon whose shape is the same as the star panel
    mask.drawPolygon([
      [0, -127],
      [43.05, -46],
      [133.05, -30],
      [70.05, 36],
      [82.05, 127],
      [0, 87],
      [-81.95, 127],
      [-69.95, 36],
      [-132.95, -30],
      [-42.95, -46],
    ]);

    mask.endFill();
    return (mask);
  }

  // ----------------------------------------------------------------------------------------------
  private setStartingState(text: string | number, scale: number): void {
    this.visible = true;

    this.alpha = 0;

    this.position.set(this._xOrig, this._yOrig);

    // starting scale is 10% of the specified scale
    this.scale.set(scale * 0.1);

    const _text = typeof(text) === 'number' ? text.toString(10) : text;
    this._text.text = _text || '';
  }

  // ----------------------------------------------------------------------------------------------
  private presentTween(endScale: number): Promise<void> {
    const target: ITweenTarget = {
      value: 0,
    };

    const tween = this.game.tweens.create(target)
      .to(
        {
          value: 1.0,
        },
        StarPanel.PRESENT_TWEENS_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
      );

    const startAlpha = this.alpha;
    const endAlpha = 1.0;
    const startScale = this.scale.x;

    tween.onUpdateCallback((_tween: Phaser.Tween, value: number) => {
      this.alpha = Phaser.Math.linear(startAlpha, endAlpha, value);
      this.scale.set(Phaser.Math.linear(startScale, endScale, value));
    });

    return new Promise((resolve) => {
      tween.onComplete.addOnce(() => {
        resolve();
      });
    });
  }

  // ----------------------------------------------------------------------------------------------
  private shineTween(shouldShine: boolean): Promise<void> {
    if (!shouldShine) {
      return (Promise.resolve());
    }

    const bling = this.game.add.image(0, 0, 'star-panel-bling', undefined, this);
    bling.anchor.set(0.5);

    const shine = this.createShine();

    const target: ITweenTarget = {
      value: 0,
    };

    const tween = this.game.tweens.create(target)
      .to(
        {
          value: 1.0,
        },
        StarPanel.SHINE_TWEENS_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
      );

    const startAlpha = bling.alpha;
    const endAlpha = 0;
    const startX = shine.x;
    const endX = -shine.x;
    tween.onUpdateCallback((_tween: Phaser.Tween, value: number) => {
      bling.alpha = Phaser.Math.linear(startAlpha, endAlpha, value);
      shine.x = Phaser.Math.linear(startX, endX, value);
    });

    return new Promise((resolve) => {
      tween.onComplete.addOnce(() => {
        resolve();
      });
    });
  }

  // ----------------------------------------------------------------------------------------------
  private unpresentTween(): void {
    this.game.tweens.create(this)
      .to(
        {
          alpha: 0,
          y: this.y - 20,
        },
        StarPanel.UNPRESENT_TWEENS_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
      );
  }
}
