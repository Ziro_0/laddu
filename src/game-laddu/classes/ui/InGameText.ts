export default class InGameText extends Phaser.Text {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly FADE_IN_TWEEN_DURATION_MS = 200;

  private static readonly MOVE_IN_TWEEN_DURATION_MS = 750;

  private static readonly MOVE_OFFSET = -50;

  private static readonly FADE_OUT_TWEEN_DURATION_MS = 250;

  private static readonly COLOR_TIMER_RATE_MS = 75;

  private static readonly COLORS = [
    '#FFFF99',
    '#FFCC00',
    '#FF9900',
    '#FFFFFF',
  ];

  private static readonly STYLE: Phaser.PhaserTextStyle = {
    font: 'sunspire',
    fontSize: 20,
    fill: '#ffff99',
    stroke: '#333333',
    strokeThickness: 4,
  };

  private _colorTimer: Phaser.Timer;

  private _colorIndex: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, text: string, group: Phaser.Group,
    fontSize?: number) {
    const style: Phaser.PhaserTextStyle = {};
    Object.keys(InGameText.STYLE).forEach((key) => {
      style[key] = InGameText.STYLE[key];
    });

    if (fontSize !== undefined) {
      style.fontSize = fontSize;
    }

    super(game, x, y, text, style);

    this.anchor.set(0.5);

    group.add(this);

    this.keepWithinMargins();

    this.present();
  }

  // ----------------------------------------------------------------------------------------------
  static Create(game: Phaser.Game, x: number, y: number, text: string,
    group: Phaser.Group, fontSize?: number): InGameText {
    return (new InGameText(game, x, y, text, group, fontSize));
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    if (this._colorTimer) {
      this._colorTimer.destroy();
      this._colorTimer = null;
    }

    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  present(): void {
    this.setStartingState();
    this.runTweens();
    this.startColorTimer();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private keepWithinMargins(): void {
    const WORLD_MARGIN = 50;
    this.left = Math.max(this.left, WORLD_MARGIN);
    this.right = Math.min(this.right, this.game.width - WORLD_MARGIN);
  }

  // ----------------------------------------------------------------------------------------------
  private nextColor(): void {
    this._colorIndex = (this._colorIndex + 1) % InGameText.COLORS.length;
    this.fill = InGameText.COLORS[this._colorIndex];
  }

  // ----------------------------------------------------------------------------------------------
  private setStartingState(): void {
    this.alpha = 0;
  }

  // ----------------------------------------------------------------------------------------------
  private startColorTimer(): void {
    this._colorIndex = 0;

    this._colorTimer = this.game.time.create();
    this._colorTimer.start();
    this._colorTimer.loop(InGameText.COLOR_TIMER_RATE_MS, () => {
      this.nextColor();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private runTweens(): void {
    const fadeInTween = this.game.tweens.create(this)
      .to(
        {
          alpha: 1.0,
        },
        InGameText.FADE_IN_TWEEN_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
      );

    const movingTween = this.game.tweens.create(this)
      .to(
        {
          y: this.y + InGameText.MOVE_OFFSET,
        },
        InGameText.MOVE_IN_TWEEN_DURATION_MS,
        Phaser.Easing.Linear.None,
      );

    const fadeOutTween = this.game.tweens.create(this)
      .to(
        {
          alpha: 0,
        },
        InGameText.FADE_OUT_TWEEN_DURATION_MS,
        Phaser.Easing.Linear.None,
      );

    fadeOutTween.onComplete.addOnce(() => {
      this.destroy();
    });

    fadeInTween.chain(movingTween, fadeOutTween);
  }
}
