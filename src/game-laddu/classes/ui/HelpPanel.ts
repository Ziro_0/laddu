import { isMobile } from '../../util/util';

export default class HelpPanel extends Phaser.Group {
  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, parent: Phaser.Group) {
    super(game, parent);
    this.init();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private init(): void {
    if (isMobile(this.game)) {
      this.initTapButtons();
    } else {
      this.initArrowButtons();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private initArrowButtons(): void {
    this.initLeftButton('button-left-arrow');
    this.initRightButton('button-right-arrow');
  }

  // ----------------------------------------------------------------------------------------------
  private initLeftButton(textureKey: string): void {
    const image = this.game.add.image(50, 549, textureKey, undefined, this);
    this.game.tweens.create(image)
      .to(
        {
          x: image.x + 25,
        },
        500,
      )
      .yoyo(true)
      .loop(true)
      .start();
  }

  // ----------------------------------------------------------------------------------------------
  private initRightButton(textureKey: string): void {
    const image = this.game.add.image(328, 549, textureKey, undefined, this);
    this.game.tweens.create(image)
      .to(
        {
          x: image.x - 25,
        },
        500,
      )
      .yoyo(true)
      .loop(true)
      .start();
  }

  // ----------------------------------------------------------------------------------------------
  private initTapButtons(): void {
    this.initLeftButton('button-tap');
    this.initRightButton('button-tap');
  }
}
