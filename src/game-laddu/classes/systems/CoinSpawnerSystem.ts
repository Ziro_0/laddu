import { Game } from '../../game';
import { irandomRange } from '../../util/util';
import Coin from '../entities/Coin';
import { CoinIds, getCoinEffects } from '../entities/data/CoinData';
import GridSystem from './GridSystem';

export default class CoinSpawnerSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly MIN_SCORES_NEEDED = 1;

  private static readonly MAX_SCORES_NEEDED = 4;

  private _game: Game;

  private _gridSystem: GridSystem;

  private _entitiesGroup: Phaser.Group;

  private _numScoresNeeded: number;

  private _curNumScores: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, gridSystem: GridSystem, entitiesGroup: Phaser.Group) {
    this._game = game;
    this._gridSystem = gridSystem;
    this._entitiesGroup = entitiesGroup;

    this._numScoresNeeded = CoinSpawnerSystem.CalcNumScoresNeeded();
    this._curNumScores = 0;
  }

  // ----------------------------------------------------------------------------------------------
  ballScored(): void {
    this._curNumScores += 1;
  }

  // ----------------------------------------------------------------------------------------------
  createCoin(coinId?: CoinIds): Coin {
    if (!coinId) {
      coinId = CoinSpawnerSystem.BuildCoinId();
    }

    const effects = getCoinEffects(coinId);
    const path = this._gridSystem.buildPath();
    const normalDurationMs = this._game.ladduConfig.coinsNormalDurationSecs * 1000;
    const warningDurationMs = this._game.ladduConfig.coinsWarningDurationSecs * 1000;
    return (Coin.Create(this._game, this._entitiesGroup, coinId, effects, normalDurationMs,
      warningDurationMs, path));
  }

  // ----------------------------------------------------------------------------------------------
  nextWave(): void {
    if (this._curNumScores < this._numScoresNeeded) {
      return;
    }

    this._numScoresNeeded = CoinSpawnerSystem.CalcNumScoresNeeded();
    this._curNumScores = 0;

    this.createCoin();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private static BuildCoinId(): CoinIds {
    const chance = Math.random();
    if (chance <= 1 / 9) {
      return (CoinIds.EASY);
    }

    if (chance <= 1 / 6) {
      return (CoinIds.TIME);
    }

    if (chance <= 1 / 3) {
      return (CoinIds.SPRINGS);
    }

    return (CoinIds.POINTS);
  }

  // ----------------------------------------------------------------------------------------------
  private static CalcNumScoresNeeded(): number {
    return (irandomRange(
      CoinSpawnerSystem.MIN_SCORES_NEEDED,
      CoinSpawnerSystem.MAX_SCORES_NEEDED,
    ));
  }
}
