import { Game } from '../../game';
import { irandom, irandomRange } from '../../util/util';
import IPath from '../entities/data/IPath';

export default class GridSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly X_START = 80;

  private static readonly Y_START = 280;

  private static readonly CELL_WIDTH = 80;

  private static readonly CELL_HEIGHT = 80;

  private static readonly NUM_COLUMNS = 5;

  private static readonly NUM_ROWS = 5;

  private _grid: Phaser.Point[][];

  private _game: Game;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    this._game = game;
    this._grid = this.buildGrid();
  }

  // ----------------------------------------------------------------------------------------------
  buildPath(): IPath {
    return (Math.random() >= 0.5 ? this.buildPathBox() : this.buildPathCircle());
  }

  // ----------------------------------------------------------------------------------------------
  drawGrid(graphics?: Phaser.Graphics): Phaser.Graphics {
    if (!graphics) {
      graphics = this._game.add.graphics();
    }

    this._grid.forEach((gridRow) => {
      gridRow.forEach((cell) => {
        this.drawGridCell(graphics, cell);
      });
    });

    return (graphics);
  }

  // ----------------------------------------------------------------------------------------------
  drawPath(path: IPath, graphics?: Phaser.Graphics): Phaser.Graphics {
    if (!path) {
      return (graphics);
    }

    if (path.isEllipse) {
      graphics = this.drawPathEllipse(path, graphics);
    } else {
      graphics = this.drawPathBox(path, graphics);
    }

    return (graphics);
  }

  // ----------------------------------------------------------------------------------------------
  getRandomCell(pointOut?: Phaser.Point): Phaser.Point {
    const column = irandom(GridSystem.NUM_COLUMNS - 1);
    const row = irandom(GridSystem.NUM_ROWS - 1);
    const cell = this.getCellAt(column, row);
    if (pointOut) {
      pointOut.set(cell.x, cell.y);
    } else {
      pointOut = new Phaser.Point(cell.x, cell.y);
    }

    return (pointOut);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private static BuildAvailCellIndexes(): number[] {
    const NUM_CELLS = GridSystem.NUM_COLUMNS * GridSystem.NUM_ROWS;
    return (Phaser.ArrayUtils.numberArray(NUM_CELLS - 1));
  }

  // ----------------------------------------------------------------------------------------------
  private buildGrid(): Phaser.Point[][] {
    const grid: Phaser.Point[][] = [];

    for (let row = 0; row < GridSystem.NUM_ROWS; row += 1) {
      const y = GridSystem.Y_START + row * GridSystem.CELL_HEIGHT;

      const gridRow: Phaser.Point[] = [];
      grid.push(gridRow);

      for (let column = 0; column < GridSystem.NUM_COLUMNS; column += 1) {
        const x = GridSystem.X_START + column * GridSystem.CELL_WIDTH;
        const point = new Phaser.Point(x, y);
        gridRow.push(point);
      }
    }

    return (grid);
  }

  // ----------------------------------------------------------------------------------------------
  private buildPathBox(): IPath {
    return (Math.random() >= 0.5 ? this.buildPathBoxLine() : this.buildPathBoxRect());
  }

  // ----------------------------------------------------------------------------------------------
  private buildPathBoxLine(): IPath {
    const points = this.buildPathBoxLinePoints();
    return GridSystem.ConstructPathObject(points, false);
  }

  // ----------------------------------------------------------------------------------------------
  private buildPathBoxLinePoints(): Phaser.Point[] {
    const points: Phaser.Point[] = [];

    const availCellIndexes = GridSystem.BuildAvailCellIndexes();
    const NUM_POINTS = 2;
    for (let lineIndex = 0; lineIndex < NUM_POINTS; lineIndex += 1) {
      const cellIndex = Phaser.ArrayUtils.removeRandomItem(availCellIndexes);
      const cell = this.getCellAt(cellIndex);
      points.push(cell);
    }

    return (points);
  }

  // ----------------------------------------------------------------------------------------------
  private buildPathBoxRect(): IPath {
    const points = this.buildPathBoxRectPoints();
    return GridSystem.ConstructPathObject(points, false);
  }

  // ----------------------------------------------------------------------------------------------
  private buildPathBoxRectPoints(): Phaser.Point[] {
    const leftCol = irandomRange(0, GridSystem.NUM_COLUMNS - 2);
    const minRightCol = leftCol + 1;
    const maxRightCol = GridSystem.NUM_COLUMNS - 1;
    const rightCol = irandomRange(minRightCol, maxRightCol);

    const topRow = irandomRange(0, GridSystem.NUM_ROWS - 2);
    const minBottomRow = topRow + 1;
    const maxBottomRow = GridSystem.NUM_ROWS - 1;
    const bottomRow = irandomRange(minBottomRow, maxBottomRow);

    const gridPointPairs = [
      [leftCol, topRow],
      [rightCol, topRow],
      [rightCol, bottomRow],
      [leftCol, bottomRow],
    ];

    const points: Phaser.Point[] = [];
    gridPointPairs.forEach((gridPointPair) => {
      const col = gridPointPair[0];
      const row = gridPointPair[1];
      const cell = this.getCellAt(col, row);
      points.push(cell);
    });

    return (points);
  }

  // ----------------------------------------------------------------------------------------------
  private buildPathCircle(): IPath {
    const centerCol = irandomRange(1, GridSystem.NUM_COLUMNS - 2);
    let ellipseWidth: number;
    if (centerCol > Math.floor(GridSystem.NUM_COLUMNS / 2)) {
      ellipseWidth = GridSystem.NUM_COLUMNS - 1 - centerCol;
    } else {
      ellipseWidth = irandomRange(1, centerCol);
    }

    const centerRow = irandomRange(1, GridSystem.NUM_ROWS - 2);
    let ellipseHeight: number;
    if (centerRow > Math.floor(GridSystem.NUM_ROWS / 2)) {
      ellipseHeight = GridSystem.NUM_ROWS - 1 - centerRow;
    } else {
      ellipseHeight = irandomRange(1, centerRow);
    }

    const path = GridSystem.ConstructPathObject([this.getCellAt(centerCol, centerRow)], true);
    path.ellipseWidth = ellipseWidth * GridSystem.CELL_WIDTH;
    path.ellipseHeight = ellipseHeight * GridSystem.CELL_HEIGHT;
    return (path);
  }

  // ----------------------------------------------------------------------------------------------
  private static ConstructPathObject(points: Phaser.Point[], isEllipse: boolean): IPath {
    const isClockwise = Math.random() >= 0.5;
    return ({
      isEllipse,
      isClockwise,
      points: isClockwise ? points : points.reverse(),
    });
  }

  // ----------------------------------------------------------------------------------------------
  private drawGridCell(graphics: Phaser.Graphics, point: Phaser.Point, color = 0x00ff00,
    alpha = 0.5, radius = 50): void {
    graphics.beginFill(color, alpha);
    graphics.drawCircle(point.x, point.y, radius);
    graphics.endFill();
  }

  // ----------------------------------------------------------------------------------------------
  private drawPathBox(path: IPath, graphics: Phaser.Graphics): Phaser.Graphics {
    if (!graphics) {
      graphics = this._game.add.graphics();
    }

    graphics.lineStyle(1, 0xff00ff, 0.5);

    const points = path.points;
    graphics.moveTo(points[0].x, points[0].y);

    for (let index = 1; index < points.length; index += 1) {
      const p = points[index];
      graphics.lineTo(p.x, p.y);
    }

    graphics.lineTo(points[0].x, points[0].y);
    return (graphics);
  }

  // ----------------------------------------------------------------------------------------------
  private drawPathEllipse(path: IPath, graphics: Phaser.Graphics): Phaser.Graphics {
    const points = path.points;
    if (!graphics) {
      graphics = this._game.add.graphics();
    }

    graphics.lineStyle(1, 0x0000ff, 0.5);
    const p = points[0];
    graphics.drawEllipse(p.x, p.y, path.ellipseWidth, path.ellipseHeight);
    return (graphics);
  }

  // ----------------------------------------------------------------------------------------------
  private getCellAt(cellIndexOrColumn: number, cellRow?: number): Phaser.Point {
    let col: number;
    let row: number;

    if (cellRow === undefined) {
      col = cellIndexOrColumn % GridSystem.NUM_COLUMNS;
      row = Math.floor(cellIndexOrColumn / GridSystem.NUM_COLUMNS);
    } else {
      col = cellIndexOrColumn;
      row = cellRow;
    }

    return (this._grid[row][col]);
  }
}
