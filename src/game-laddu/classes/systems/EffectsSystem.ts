import { Game } from '../../game';
import Listeners from '../../Listeners';
import { addPoints, listenerCallback } from '../../util/util';
import AudioPlayer from '../audio/AudioPlayer';
import Ball from '../entities/Ball';
import Basket from '../entities/Basket';
import Bumper from '../entities/Bumper';
import { CoinEffects, CoinIds, getCoinEffectSoundKey } from '../entities/data/CoinData';
import CameraSpringEffect from '../entities/effects/CameraSpringEffect';
import Spring from '../entities/Spring';
import Wall from '../entities/Wall';
import LostLivesHeart from '../ui/LostLivesHeart';
import UiFlash from '../ui/UiFlash';

export default class EffectsSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _springs: Spring[];

  private _basket: Basket;

  private _entitiesGroup: Phaser.Group;

  private _uiGroup: Phaser.Group;

  private _leftWall: Wall;

  private _rightWall: Wall;

  private _cameraSpringEffect: CameraSpringEffect;

  private _game: Game;

  private _ball: Ball;

  private _audio: AudioPlayer;

  private _pointsMultiplier: number;

  private _numEasyWavesRemaining: number;

  private _viewOffset: number;

  private _numConsecLosses: number;

  private _wasSpringHitThisWave: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, viewOffset: number, entitiesGroup: Phaser.Group, uiGroup: Phaser.Group,
    audio: AudioPlayer) {
    this._game = game;
    this._viewOffset = viewOffset;
    this._entitiesGroup = entitiesGroup;
    this._uiGroup = uiGroup;
    this._audio = audio;

    const MAX_SPRINGS = 3;
    this._springs = new Array(MAX_SPRINGS);

    this._cameraSpringEffect = new CameraSpringEffect(this._game);

    this._pointsMultiplier = 1;
    this._numEasyWavesRemaining = 0;
    this._numConsecLosses = 0;
  }

  // ----------------------------------------------------------------------------------------------
  get ball(): Ball {
    return (this._ball);
  }

  // ----------------------------------------------------------------------------------------------
  set ball(value: Ball) {
    this._ball = value;
  }

  // ----------------------------------------------------------------------------------------------
  get basket(): Basket {
    return (this._basket);
  }

  // ----------------------------------------------------------------------------------------------
  createSprings(): Spring[] {
    const springs: Spring[] = [];

    let spring = this.createSpring();
    while (spring) {
      springs.push(spring);
      spring = this.createSpring();
    }

    return (springs);
  }

  // ----------------------------------------------------------------------------------------------
  enableSprings(enabled: boolean): void {
    this._springs.forEach((spring) => {
      if (spring) {
        spring.setEnabled(enabled);
      }
    });
  }

  // ----------------------------------------------------------------------------------------------
  initStaticEntities(): void {
    const viewOffset = this._viewOffset;
    const group = this._entitiesGroup;

    this._leftWall = new Wall(this._game, true, viewOffset, group);
    this._rightWall = new Wall(this._game, false, viewOffset, group);
    this._basket = new Basket(this._game, viewOffset, group);
  }

  // ----------------------------------------------------------------------------------------------
  get isEasyModeActive(): boolean {
    return (this._numEasyWavesRemaining > 0);
  }

  // ----------------------------------------------------------------------------------------------
  get leftWall(): Wall {
    return (this._leftWall);
  }

  // ----------------------------------------------------------------------------------------------
  nextWave(wasPrevWaveSuccessful?: boolean): void {
    this._wasSpringHitThisWave = false;

    this.nextWavePointsMultiplier(wasPrevWaveSuccessful);

    if (this._numEasyWavesRemaining > 0) {
      this._numEasyWavesRemaining -= 1;
    }

    if (!this.isEasyModeActive) {
      this.morphWalls(wasPrevWaveSuccessful);
      this._basket.nextWave(wasPrevWaveSuccessful);
      this._basket.isLarge = false;

      if (wasPrevWaveSuccessful === false) {
        addPoints(this._game, -this._game.ladduConfig.pointsLost);
      }
    }

    this.enableSprings(true);
  }

  // ----------------------------------------------------------------------------------------------
  get pointsMultiplier(): number {
    return (this._pointsMultiplier);
  }

  // ----------------------------------------------------------------------------------------------
  get rightWall(): Wall {
    return (this._rightWall);
  }

  // ----------------------------------------------------------------------------------------------
  spring(spring: Spring): void {
    if (!spring) {
      return;
    }

    if (this._wasSpringHitThisWave) {
      return;
    }

    this._wasSpringHitThisWave = true;

    spring.bump(this.ball);

    this.enableSprings(false);
    this._audio.play('snd-spring');

    this._cameraSpringEffect.start();
  }

  // ----------------------------------------------------------------------------------------------
  start(id: CoinIds, effects: CoinEffects[]): void {
    if (!effects) {
      return;
    }

    this.playEffectSond(id);

    effects.forEach((effect) => {
      switch (effect) {
        case CoinEffects.EASY:
          this.processEasy();
          break;

        case CoinEffects.POINTS:
          this.processPoints();
          break;

        case CoinEffects.POINTS_MULTIPLIER:
          this.processPointsMultiplier();
          break;

        case CoinEffects.SPRINGS:
          this.processSprings();
          break;

        case CoinEffects.TIME:
          this.processTime();
          break;
      }
    });
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private createSpring(): Spring {
    const freeIndex = this._springs.findIndex((spring) => !spring);
    if (freeIndex === -1) {
      return (null);
    }

    const chance = Math.random();
    let hits: number;
    if (chance <= 1 / 9) {
      hits = 4;
    } else if (chance <= 1 / 6) {
      hits = 3;
    } else if (chance <= 1 / 3) {
      hits = 2;
    } else {
      hits = 1;
    }

    const spring = Spring.Create(this._game, this._entitiesGroup, freeIndex, hits);
    spring.events.onDestroy.addOnce(() => {
      this._springs[freeIndex] = null;
    });

    this._springs[freeIndex] = spring;
    return (spring);
  }

  // ----------------------------------------------------------------------------------------------
  private morphWalls(wasPrevWaveSuccessful?: boolean): void {
    if (wasPrevWaveSuccessful === undefined) {
      return;
    }

    let wallIsSpiking: Wall;

    // morphWalls should only be called during a new wave. During this time, all
    // spiked walls become un-spiked. However, if the previous wave was successful, a wall that
    // was already un-spiked, could potentially become spiked.

    if (this.morphWall(this._leftWall, wasPrevWaveSuccessful)) {
      wallIsSpiking = this._leftWall;
    }

    if (this.morphWall(this._rightWall, wasPrevWaveSuccessful)) {
      wallIsSpiking = this._rightWall;
    }

    if (wallIsSpiking) {
      wallIsSpiking.onPresent.addOnce((isSpiked: boolean) => {
        if (isSpiked) {
          this._audio.play('snd-wall-spikes-appeared');
        }
      });
    }
  }

  // ----------------------------------------------------------------------------------------------
  private morphWall(wall: Wall, wasPrevWaveSuccessful?: boolean): boolean {
    if (wall.isSpiked) {
      return (wall.morph(false));
    }

    if (!wasPrevWaveSuccessful) {
      return (false);
    }

    const CHANCE_FOR_WALL_TO_MORPH = 0.25;
    if (Math.random() < CHANCE_FOR_WALL_TO_MORPH) {
      return (wall.morph());
    }

    return (false);
  }

  // ----------------------------------------------------------------------------------------------
  private neutralizeWalls(): void {
    this._leftWall.morph(false);
    this._rightWall.morph(false);
  }

  // ----------------------------------------------------------------------------------------------
  private nextWavePointsMultiplier(wasPrevWaveSuccessful?: boolean): void {
    if (wasPrevWaveSuccessful !== false) {
      this._numConsecLosses = 0;
      return;
    }

    if (this.isEasyModeActive) {
      return
    }

    this._numConsecLosses += 1;
    if (this._numConsecLosses < 2) {
      return;
    }

    this._numConsecLosses = 0;

    const prevMultiplier = this._pointsMultiplier;
    this._pointsMultiplier = Math.max(1, this._pointsMultiplier - 1);
    listenerCallback(this._game, Listeners.SET_HEARTS, this._pointsMultiplier);

    if (prevMultiplier > this._pointsMultiplier) {
      this.presentLostLivesHeart();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private playEffectSond(id: CoinIds): void {
    const effectSoundKey = getCoinEffectSoundKey(id);
    this._audio.play(effectSoundKey);
  }

  // ----------------------------------------------------------------------------------------------
  private presentEasyScreenFlash(): void {
    UiFlash.Create(this._game, {
      color: 0x80ffff,
      numFlashes: 4,
      alpha: 0.6
    }, this._uiGroup);
  }

  // ----------------------------------------------------------------------------------------------
  private presentLostLivesHeart(): void {
    const VALUE = -1;
    const heart = new LostLivesHeart(this._game, 400, 80 + this._viewOffset, VALUE);
    this._uiGroup.add(heart);
  }

  // ----------------------------------------------------------------------------------------------
  private processEasy(): void {
    this._numEasyWavesRemaining = 10;
    this._numConsecLosses = 0;

    this.neutralizeWalls();
    this.removeBumpers();
    this._basket.stopMoveContinuous();
    this._basket.isLarge = true;

    this.presentEasyScreenFlash();
  }

  // ----------------------------------------------------------------------------------------------
  private processPoints(): void {
    addPoints(this._game, this._game.ladduConfig.effectPointsAward);
  }

  // ----------------------------------------------------------------------------------------------
  private processPointsMultiplier(): void {
    this._pointsMultiplier += 1;
    listenerCallback(this._game, Listeners.SET_HEARTS, this._pointsMultiplier);
  }

  // ----------------------------------------------------------------------------------------------
  private processSprings(): void {
    this.createSprings();
  }

  // ----------------------------------------------------------------------------------------------
  private processTime(): void {
    const timeAdded = this._game.ladduConfig.effectTimeAdded;
    listenerCallback(this._game, Listeners.SET_SECONDS, timeAdded, true);
  }

  // ----------------------------------------------------------------------------------------------
  private removeBumpers(): void {
    this._entitiesGroup.forEach((entity: any) => {
      if (entity instanceof Bumper) {
        entity.unpresent();
      }
    });
  }
}
