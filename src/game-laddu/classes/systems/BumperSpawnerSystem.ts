import { IPointsResult } from '../../../pages/home/home';
import { Game } from '../../game';
import Listeners from '../../Listeners';
import { irandomRange, listenerCallback } from '../../util/util';
import AudioPlayer from '../audio/AudioPlayer';
import Bumper from '../entities/Bumper';
import EffectsSystem from './EffectsSystem';
import GridSystem from './GridSystem';

// ================================================================================================
interface ICreateParams {
  isMoving?: boolean;
  isLarge?: boolean;
}

export default class BumperSpawnerSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _game: Game;

  private _gridSystem: GridSystem;

  private _effectsSystem: EffectsSystem;

  private _audio: AudioPlayer;

  private _entitiesGroup: Phaser.Group;

  private _bumper: Bumper;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, gridSystem: GridSystem, effectsSystem: EffectsSystem,
    audio: AudioPlayer, entitiesGroup: Phaser.Group) {
    this._game = game;
    this._gridSystem = gridSystem;
    this._effectsSystem = effectsSystem;
    this._audio = audio;
    this._entitiesGroup = entitiesGroup;
  }

  // ----------------------------------------------------------------------------------------------
  get bumper(): Bumper {
    return (this._bumper);
  }

  // ----------------------------------------------------------------------------------------------
  createBumper(isMoving?: boolean, isLarge?: boolean): Bumper {
    if (this._bumper) {
      return (null);
    }

    const path = isMoving ? this._gridSystem.buildPath(): null;
    const scale = isLarge ? 2.0 : 1.0;
    const hits = BumperSpawnerSystem.CalcHits(isMoving, isLarge);

    this._bumper = Bumper.Create(this._game, this._entitiesGroup, this._audio, hits, path, scale);
    this._bumper.events.onDestroy.addOnce(() => {
      this._bumper = null;
    });

    if (!isMoving) {
      const cell = this._gridSystem.getRandomCell();
      this._bumper.physicsBody.x = cell.x;
      this._bumper.physicsBody.y = cell.y;
    }

    return (this._bumper);
  }

  // ----------------------------------------------------------------------------------------------
  nextWave(wasPrevWaveSuccessful?: boolean): void {
    if (wasPrevWaveSuccessful === undefined) {
      return;
    }

    if (this._effectsSystem.isEasyModeActive) {
      return;
    }

    if (!wasPrevWaveSuccessful) {
      return;
    }

    if (this._bumper) {
      this.unpresentHitBumper();
    } else {
      this.createBumperChance();
    }
  }

  // ----------------------------------------------------------------------------------------------
  unpresentBumper(): void {
    if (this._bumper) {
      this._bumper.unpresent();
      this._bumper = null;
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private static CalcHits(isMoving: boolean, isLarge: boolean): number {
    let hits = 1;

    if (isLarge) {
      hits += irandomRange(1, 2);
    }

    if (isMoving) {
      hits += irandomRange(0, 1);
    }

    return (hits);
  }

  // ----------------------------------------------------------------------------------------------
  private createBumperChance(): void {
    const points: IPointsResult = {};
    listenerCallback(this._game, Listeners.GET_POINTS, points);

    if (points.value >= 30) {
      const params = this.createParams(points);
      this.createBumper(params.isMoving, params.isLarge);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private createParams(points: IPointsResult): ICreateParams {
    const params: ICreateParams = {
      isLarge: false,
      isMoving: false,
    };

    if (points.value >= 120) {
      params.isLarge = Math.random() < 0.5;
      params.isMoving = Math.random() < 0.5;
    } else if (points.value >= 90) {
      params.isMoving = Math.random() < 0.5;
    } else if (points.value >= 60) {
      params.isLarge = Math.random() < 0.5;
    }

    return (params);
  }

  // ----------------------------------------------------------------------------------------------
  private unpresentHitBumper(): void {
    this._bumper.hits -= 1;
    if (this._bumper.hits <= 0) {
      this.unpresentBumper();
    }
  }
}
