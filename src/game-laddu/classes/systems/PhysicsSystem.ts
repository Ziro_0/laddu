import { Game } from '../../game';
import Ball from '../entities/Ball';

export default class PhysicsSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static DebugBodies: boolean;

  private _game: Game;

  private _cbmPoint: Phaser.Point;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, shouldCreate = true) {
    this._game = game;

    if (shouldCreate) {
      this.create();
    }
  }

  // ----------------------------------------------------------------------------------------------
  calcBallMagnitude(ball: Ball): number {
    if (!ball || !ball.physicsBody) {
      return (0);
    }

    if (!this._cbmPoint) {
      this._cbmPoint = new Phaser.Point();
    }

    const v = ball.physicsBody.velocity;
    this._cbmPoint.set(v.x, v.y);
    return(this._cbmPoint.getMagnitude());
  }

  // ----------------------------------------------------------------------------------------------
  create(): void {
    const game = this._game;
    const config = game.ladduConfig;

    PhysicsSystem.DebugBodies = config.debugShowPhysicsBodies;

    game.physics.startSystem(Phaser.Physics.P2JS);
    game.physics.p2.gravity.y = config.gravity;
    game.physics.p2.restitution = config.restitution;
    game.physics.p2.setBoundsToWorld(true, true, false, true);

    this._cbmPoint = new Phaser.Point();
  }

  // ----------------------------------------------------------------------------------------------
  interpolateBallMagnitude(ball: Ball, min: number, max: number): number {
    const MIN_MAG_THRESHOLD = 500;
    const MAX_MAG_THRESHOLD = 1500;
    const mag = this.calcBallMagnitude(ball);
    const ratio = (mag - MIN_MAG_THRESHOLD) / (MAX_MAG_THRESHOLD - MIN_MAG_THRESHOLD);
    const clampedRatio = Phaser.Math.clamp(ratio, 0, 1);
    return (Phaser.Math.linear(min, max, clampedRatio));
  }
}
