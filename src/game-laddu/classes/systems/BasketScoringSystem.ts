import { Game } from '../../game';
import { addPoints } from '../../util/util';
import ILadduGameConfig from '../entities/data/ILadduGameConfig';
import BumperSpawnerSystem from './BumperSpawnerSystem';
import EffectsSystem from './EffectsSystem';

// ==============================================================================================
export interface IScoringData {
  points: number;
  isQuick: boolean;
}

interface ITimerRate {
  value?: number;
}

export default class BasketScoringSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _game: Game;

  private _effectsSystem: EffectsSystem;

  private _bumperSpawnerSystem: BumperSpawnerSystem;

  private _waveStartTimeMs: number;

  private _numOfRebounds: number;

  private _numOfReboundsPerPoints: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, effectsSystem: EffectsSystem,
    bumperSpawnerSystem: BumperSpawnerSystem) {
    this._game = game;
    this._effectsSystem = effectsSystem;
    this._bumperSpawnerSystem = bumperSpawnerSystem;
    this._numOfReboundsPerPoints = 0;
  }

  // ----------------------------------------------------------------------------------------------
  nextWave(): void {
    this._waveStartTimeMs = Date.now();
    this._numOfRebounds = 0;
  }

  // ----------------------------------------------------------------------------------------------
  rebound(): boolean {
    this._numOfRebounds += 1;
    return (this.upateReboundsPerPoints());
  }

  // ----------------------------------------------------------------------------------------------
  score(): IScoringData {
    const timerRate: ITimerRate = {};
    const timeBonusPoints = this.calcTimeBonusPoints(timerRate);
    const pointsPerBallScore = this.config.pointsPerBallScore;
    const bumperBonus = this.calcBumperBonusPoints();
    const wallsBonus = this.calcWallsBonusPoints();
    const movingBasketBonus = this.calcMovingBasketBonus();

    const pointsMultiplier = this._effectsSystem.pointsMultiplier;

    const subTotalPoints = pointsPerBallScore + timeBonusPoints + this._numOfRebounds
      + bumperBonus + wallsBonus + movingBasketBonus;

    const finalPoints = subTotalPoints * pointsMultiplier;

    /*
    console.log('--------------');
    console.log('pointsPerBallScore', pointsPerBallScore);
    console.log('timeBonusPoints', timeBonusPoints);
    console.log('numOfRebounds', this._numOfRebounds);
    console.log('bumperBonus', bumperBonus);
    console.log('wallsBonus', wallsBonus);
    console.log('movingBasketBonus', movingBasketBonus);
    console.log('subTotalPoints', subTotalPoints);
    console.log('pointsMultiplier', pointsMultiplier);
    console.log('finalPoints', finalPoints);
    console.log('--------------');
    */

    addPoints(this._game, finalPoints);

    return ({
      points: finalPoints,
      isQuick: timerRate.value >= 0.45,
    });
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private calcBumperBonusPoints(): number {
    const bumper = this._bumperSpawnerSystem.bumper;
    if (!bumper) {
      return (0);
    }

    const config = this._game.ladduConfig;
    if (bumper.isMoving) {
      return (bumper.isLarge ? config.bumperPointsLargeMoving : config.bumperPointsSmallMoving);
    }

    return (bumper.isLarge ? config.bumperPointsLargeStill : config.bumperPointsSmallStill);
  }

  // ----------------------------------------------------------------------------------------------
  private calcMovingBasketBonus(): number {
    return (this._effectsSystem.basket.isMovingContinuous
      ? this.config.movingBasketBonusPoints
      : 0);
  }

  // ----------------------------------------------------------------------------------------------
  private calcTimeBonusPoints(timerRateOut: ITimerRate): number {
    const timeBonusGraceMs = this.config.timeBonusGraceSeconds * 1000;
    if (timeBonusGraceMs <= 0) {
      return (0);
    }

    const waveElapsedTimeMs = Date.now() - this._waveStartTimeMs;
    const timerRate = Phaser.Math.clamp(1.0 - (waveElapsedTimeMs / timeBonusGraceMs), 0, 1.0);
    const maxTimeBonusPoints = this.config.maxTimeBonusPoints;

    timerRateOut.value = timerRate;

    return (Math.floor(Phaser.Math.linear(0, maxTimeBonusPoints, timerRate)));
  }

  // ----------------------------------------------------------------------------------------------
  private calcWallsBonusPoints(): number {
    const WALL_BONUS_POINTS = this.config.wallBonusPoints;
    let bonusPoints = 0;

    if (this._effectsSystem.leftWall.isSpiked) {
      bonusPoints += WALL_BONUS_POINTS;
    }

    if (this._effectsSystem.rightWall.isSpiked) {
      bonusPoints += WALL_BONUS_POINTS;
    }

    return (bonusPoints);
  }

  // ----------------------------------------------------------------------------------------------
  private get config(): ILadduGameConfig {
    return (this._game.ladduConfig);
  }

  // ----------------------------------------------------------------------------------------------
  private upateReboundsPerPoints(): boolean {
    const config = this._game.ladduConfig;

    this._numOfReboundsPerPoints += 1;
    if (this._numOfReboundsPerPoints < config.numReboundsPerPoints) {
      return (false);
    }

    this._numOfReboundsPerPoints = 0;
    const finalPoints = config.reboundPoints * this._effectsSystem.pointsMultiplier;
    addPoints(this._game, finalPoints);
    return (true);
  }
}
