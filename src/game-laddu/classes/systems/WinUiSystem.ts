import { Game } from '../../game';
import AudioPlayer from '../audio/AudioPlayer';
import StarPanel from '../ui/StarPanel';
import { IScoringData } from './BasketScoringSystem';

export default class WinUiSystem {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static AWARDS_UI_SCALE_SMALL = 0.5;

  private static AWARDS_UI_SCALE_MEDIUM = 0.8;

  private static AWARDS_UI_SCALE_LARGE = 1.1;

  private _game: Game;

  private _starPanel: StarPanel;

  private _audioPlayer: AudioPlayer;

  private _group: Phaser.Group;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game, audioPlayer: AudioPlayer, group: Phaser.Group) {
    this._game = game;
    this._audioPlayer = audioPlayer;
    this._group = group;

    this._starPanel = new StarPanel(this._game, this._group);
  }

  // ----------------------------------------------------------------------------------------------
  present(scoringData: IScoringData): void {
    const points = scoringData.points;
    const config = this._game.ladduConfig;

    if (points <= config.awardUiPointsSmall) {
      this.presentSmallWin(points);
    } else if (points <= config.awardUiPointsMedium) {
      this.presentMediumWin(points);
    } else {
      this.presentLargeWin(points);
    }
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private presentMediumWin(points: number): void {
    this._audioPlayer.play('snd-win-medium');
    this._starPanel.present(points, WinUiSystem.AWARDS_UI_SCALE_MEDIUM);
  }

  // ----------------------------------------------------------------------------------------------
  private presentLargeWin(points: number): void {
    this._audioPlayer.play('snd-win-large');
    const SHOULD_SHINE = true;
    this._starPanel.present(points, WinUiSystem.AWARDS_UI_SCALE_LARGE, SHOULD_SHINE);
  }

  // ----------------------------------------------------------------------------------------------
  private presentSmallWin(points: number): void {
    this._audioPlayer.play('snd-win-small');
    this._starPanel.present(points, WinUiSystem.AWARDS_UI_SCALE_SMALL);
  }
}
