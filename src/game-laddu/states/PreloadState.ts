export default class PreloadState {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly BASE_PATH = 'assets/game-laddu/';

  private static readonly SOUNDS_PATH = 'assets/game-laddu/sounds/';

  private game: Phaser.Game;

  private preloadBar: Phaser.Image;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this.game = game;
  }

  // ----------------------------------------------------------------------------------------------
  create(): void {
    this.game.state.start('MainMenuState');
  }

  // ----------------------------------------------------------------------------------------------
  preload(): void {
    this.preloadBar = this.game.add.image(
      this.game.world.centerX,
      this.game.world.centerY,
      'preload_bar',
    );

    this.preloadBar.anchor.set(0.5);

    this.game.load.setPreloadSprite(this.preloadBar);

    // load assets
    this.loadSpritesheets();
    this.loadImages();
    this.loadSounds();
    this.loadFonts();
    this.loadJsons();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private loadFont(key: string): void {
    const path = `${PreloadState.BASE_PATH}`;
    this.game.load.bitmapFont(key, `${path}${key}.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFontBfg(key: string): void {
    // Loads fonts created by Bitmap Font Generator. BFG appends a "_x"-like template onto the 
    // ends of its image files, which doesn't exactly match key (which doesn't use the template).
    const path = `${PreloadState.BASE_PATH}`;
    this.game.load.bitmapFont(key, `${path}${key}_0.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFonts(): void {
    [
    ].forEach((key) => {
      this.loadFont(key);
    });

    [].forEach((key) => {
      this.loadFontBfg(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadImage(key: string): void {
    this.game.load.image(key, `${PreloadState.BASE_PATH}${key}.png`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadImages(): void {
    [
      'background',
      'ball',
      'basket',
      'basket-front',
      'bumper-impact',
      'cut-rope-effect-ring',
      'cut-rope-effect-star',
      'ground',
      'incorrect-orientation-message',
      'lost-lives-heart',
      'rope-base',
      'rope-cutter',
      'rope-segment',
      'star-panel',
      'star-panel-bling',
      'wall',
      'wall-spikes',
    ].forEach((key) => {
      this.loadImage(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadJson(key: string): void {
    this.game.load.json(key, `${PreloadState.BASE_PATH}${key}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadJsons(): void {
    [
    ].forEach((key) => {
      this.loadJson(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadSound(key: string): void {
    const baseUrl = `${PreloadState.SOUNDS_PATH}${key}`;
    const mp3Url = `${baseUrl}.mp3`;
    const oggUrl = `${baseUrl}.ogg`;
    const urls = [
      mp3Url,
      oggUrl,
    ];

    this.game.load.audio(key, urls);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSounds(): void {
    [
      'snd-ball-deff0',
      'snd-ball-deff1',
      'snd-ball-rebound-basket',
      'snd-ball-rebound-wall',
      'snd-bumper',
      'snd-bumper-large',
      'snd-effect-easy',
      'snd-effect-points',
      'snd-effect-springs',
      'snd-effect-time',
      'snd-rope-cut-impact',
      'snd-rope-cut-start',
      'snd-spring',
      'snd-wall-spikes-appeared',
      'snd-win-large',
      'snd-win-medium',
      'snd-win-small',
    ].forEach((key) => {
      this.loadSound(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadSpritesheet(key: string, frameWidth: number, frameHeight: number): void {
    const url = `${PreloadState.BASE_PATH}${key}.png`;
    this.game.load.spritesheet(key, url, frameWidth, frameHeight);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSpritesheets(): void {
    [
      ['ball-deff', 100, 100],
      ['bumper', 63, 64],
      ['coin-blue-black', 57, 32],
      ['coin-gold-white', 57, 32],
      ['coin-green-cyan', 57, 32],
      ['coin-pink-cyan', 57, 32],
      ['present-coin-effect', 200, 200],
      ['spring', 140, 45],
    ].forEach((data) => {
      this.loadSpritesheet(<string> data[0], <number> data[1], <number> data[2]);
    });
  }
}