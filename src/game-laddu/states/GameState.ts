import AudioPlayer from '../classes/audio/AudioPlayer';
import LostLivesHeart from '../classes/ui/LostLivesHeart';
import PlayerInputs from '../classes/ui/PlayerInputs';
import { Game } from '../game';
import Listeners from '../Listeners';
import Ball from '../classes/entities/Ball';
import GameRopeEffect from '../classes/entities/effects/GameRopeEffect';
import PhysicsSystem from '../classes/systems/PhysicsSystem';
import RopeCutterEffect from '../classes/entities/effects/RopeCutterEffect';
import ImpactEffect from '../classes/entities/effects/ImpactEffect';
import Wall from '../classes/entities/Wall';
import Ground from '../classes/entities/Ground';
import ILadduGameConfig from '../classes/entities/data/ILadduGameConfig';
import Coin from '../classes/entities/Coin';
import EffectsSystem from '../classes/systems/EffectsSystem';
import Bumper from '../classes/entities/Bumper';
import Spring from '../classes/entities/Spring';
import BasketScoringSystem from '../classes/systems/BasketScoringSystem';
import WinUiSystem from '../classes/systems/WinUiSystem';
import InGameText from '../classes/ui/InGameText';
import CoinSpawnerSystem from '../classes/systems/CoinSpawnerSystem';
import GridSystem from '../classes/systems/GridSystem';
import { getCoinEffectText } from '../classes/entities/data/CoinData';
import BumperSpawnerSystem from '../classes/systems/BumperSpawnerSystem';
import getRandomRopeMetrics from '../classes/entities/data/RopeMetrics';
import { listenerCallback } from '../util/util';
import UiFlash from '../classes/ui/UiFlash';

export default class GameState extends Phaser.State {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _audioPlayer: AudioPlayer;

  private _playerInputs: PlayerInputs;

  private _backgroundGroup: Phaser.Group;

  private _entitiesGroup: Phaser.Group;

  private _uiGroup: Phaser.Group;

  private _incorrectOrientationImage: Phaser.Group;

  private _physicsSystem: PhysicsSystem;

  private _effectsSystem: EffectsSystem;

  private _basketScoringSystem: BasketScoringSystem;

  private _winUiSystem: WinUiSystem;

  private _coinSpawnerSystem: CoinSpawnerSystem;

  private _bumperSpawnerSystem: BumperSpawnerSystem;

  private _gridSystem: GridSystem;

  private _ball: Ball;

  private _gameRope: GameRopeEffect;

  private _ropeCutter: RopeCutterEffect;

  private _ground: Ground;

  private _out: Ground;

  private _timer: Phaser.Timer;

  private _lastBasketReboundTimeMs: number;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    super();
  }

  // ----------------------------------------------------------------------------------------------
  get audio(): AudioPlayer {
    return (this._audioPlayer);
  }

  // ----------------------------------------------------------------------------------------------
  create() {
    console.log('Game state [create] started');

    this.game.stage.backgroundColor = '#000000';

    setTimeout(() => {
      this.initOrientation();

      this.initTimer();

      this.initAudioPlayer();

      this.initGroups();

      this.initSystems();

      this.initUi();
      this.initEntities();

      this.startGame();

      listenerCallback(this.gameObject, Listeners.READY, this.game);
    }, 10);
  }

  // ----------------------------------------------------------------------------------------------
  get gameObject(): Game {
    return (<Game>this.game);
  }

  // ----------------------------------------------------------------------------------------------
  shutdown(): void {
    this.audio.dispose();
    this._playerInputs.destroy();
    this.game = null;
  }

  // ----------------------------------------------------------------------------------------------
  get timer(): Phaser.Timer {
    return (this._timer);
  }

  // ----------------------------------------------------------------------------------------------
  get uiGroup(): Phaser.Group {
    return (this._uiGroup);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private ballDeff(shouldScreenTremor = false): void {
    const volume = this.interpolateBallMagnitudeForVolume();
    const intensity = this.interpolateBallMagnitudeForTremor();

    if (this._ball) {
      this._ball.def();
    } else {
      console.trace('wtf');
    }

    if (shouldScreenTremor) {
      this.presentScreenTremor(intensity);
    }

    this.audio.play(['snd-ball-deff0', 'snd-ball-deff1'], undefined, undefined, volume);

    this.nextWave(false);
  }

  // ----------------------------------------------------------------------------------------------
  private interpolateBallMagnitude(min: number, max: number): number {
    return (this._physicsSystem.interpolateBallMagnitude(this._ball, min, max));
  }

  // ----------------------------------------------------------------------------------------------
  private interpolateBallMagnitudeForTremor(): number {
    const MIN_INTNESITY = 0.01;
    const MAX_INTNESITY = 0.025;
    return (this.interpolateBallMagnitude(MIN_INTNESITY, MAX_INTNESITY));
  }

  // ----------------------------------------------------------------------------------------------
  private interpolateBallMagnitudeForVolume(): number {
    const MIN_VOL = 0.25;
    const MAX_VOL = 1.0;
    return (this.interpolateBallMagnitude(MIN_VOL, MAX_VOL));
  }

  // ----------------------------------------------------------------------------------------------
  private ballRebounds(soundKey: string, contactEquations: p2.ContactEquation[]): void {
    const mag = this.interpolateBallMagnitudeForVolume();

    this.audio.play(soundKey, null, null, mag);

    this.presentBallReboundImpact(mag, contactEquations);

    if (this._basketScoringSystem.rebound()) {
      InGameText.Create(this.game, this._ball.x, this._ball.y - this._ball.height,
        'REBOUND!', this._uiGroup);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private ballReboundsWall(contactEquations: p2.ContactEquation[]): void {
    this.ballRebounds('snd-ball-rebound-wall', contactEquations);
  }

  // ----------------------------------------------------------------------------------------------
  presentBallReboundImpact(mag: number, equations: p2.ContactEquation[]): void {
    const eq = equations ? equations[0] : null;
    let cx: number;
    let cy: number;

    if (eq) {
      // cx, cy calculations are from 'Tom Atom':
      // https://www.html5gamedevs.com/topic/26125-p2-physics-contact-point-between-2-bodies/?do=findComment&comment=152105
      const pos = eq.bodyA.position;
      const pt = eq.contactPointA;
      cx = this.game.physics.p2.mpxi(pos[0] + pt[0]);
      cy = this.game.physics.p2.mpxi(pos[1] + pt[1]);
    } else {
      // fallback, just in case...
      cx = this._ball.x;
      cy = this._ball.y;
    }


    const minScale = 0.75 * mag;
    const maxScale = 1.5 * mag;
    this.impactEffect(cx, cy, minScale, maxScale);
  }

  // ----------------------------------------------------------------------------------------------
  private calcViewOffset(): number {
    const headerBarDiv = document.getElementById('headerBar');
    return (headerBarDiv ? headerBarDiv.clientHeight * this.game.scale.scaleFactor.y : 0);
  }

  // ----------------------------------------------------------------------------------------------
  private collectCoin(coin: Coin): void {
    this._effectsSystem.start(coin.id, coin.effects);

    const effectText = getCoinEffectText(coin.id);
    const FONT_SIZE = 30;
    InGameText.Create(this.game, coin.x, coin.y - coin.height, effectText, this._uiGroup,
      FONT_SIZE);

    coin.unpresent();
  }

  // ----------------------------------------------------------------------------------------------
  private get config(): ILadduGameConfig {
    return (this.gameObject.ladduConfig);
  }

  // ----------------------------------------------------------------------------------------------
  private cutRope(): void {
    // get the position of the cuting segment _before_ cutting the rope (:
    const position = this.cuttingSegmentPosition();

    this._gameRope.cut();

    this.impactEffect(position.x, position.y, 0.5, 2.0);

    this.audio.play('snd-rope-cut-impact');
  }

  // ----------------------------------------------------------------------------------------------
  private cuttingSegmentPosition(): Phaser.Point {
    const index = Math.max(this._gameRope.ropeLength - 6, 0);
    const position = this._gameRope.getSegmentPositionOf(index);
    return (position);
  }

  // ----------------------------------------------------------------------------------------------
  private async fire(): Promise<void> {
    if (!this._gameRope.isReadyToFire) {
      return;
    }

    const position = this.cuttingSegmentPosition();
    this._ropeCutter.y = position.y;

    this._ropeCutter.start()
      .then(() => {
        const ball = this._gameRope.fire();
        if (ball) {
          this.setBall(ball);
        }

        this.cutRope();
      });

    this.audio.play('snd-rope-cut-start');
  }

  // ----------------------------------------------------------------------------------------------
  private hideIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      this._incorrectOrientationImage.destroy();
      this._incorrectOrientationImage = null;
    }

    this.game.paused = false;
  }

  // ----------------------------------------------------------------------------------------------
  private impactEffect(x: number, y: number, startScale: number, endScale: number): ImpactEffect {
    const effect = new ImpactEffect(this.game, x, y, this._entitiesGroup, startScale, endScale);
    return (effect);
  }

  // ----------------------------------------------------------------------------------------------
  private initAudioPlayer(): void {
    this._audioPlayer = new AudioPlayer(this.game);
  }

  // ----------------------------------------------------------------------------------------------
  private initBackground(): void {
    const viewOffset = this.calcViewOffset();
    const image = this.add.image(0, viewOffset, 'background', undefined, this._backgroundGroup);
    image.width = this.game.width;
    image.height = this.game.height;
  }

  // ----------------------------------------------------------------------------------------------
  private initEntities(): void {
    const viewOffset = this.calcViewOffset();

    this._ground = new Ground(this.game, this._entitiesGroup);
    this._ground.physicsBody.y = this.game.height + this._ground.height / 2;

    const OUT_OF_BOUNDS_OFFSET = -30;
    this._out = new Ground(this.game, this._entitiesGroup);
    this._out.physicsBody.y = OUT_OF_BOUNDS_OFFSET + viewOffset;

    this._effectsSystem.initStaticEntities();

    this._gameRope = new GameRopeEffect(this.game, viewOffset, this._timer, this._entitiesGroup);
    this._gameRope.swingDurationMs = this.config.ropeSwingStartDurationSecs * 1000;

    const ropeBase = this.game.add.image(this.game.world.centerX, 0 + viewOffset, 'rope-base');
    ropeBase.anchor.set(0.5);
    this._entitiesGroup.add(ropeBase);

    this._ropeCutter = new RopeCutterEffect(this.game, this._entitiesGroup);
  }

  // ----------------------------------------------------------------------------------------------
  private initOrientation(): void {
    this.scale.onOrientationChange.add((scale: Phaser.ScaleManager,
      prevOrientation: string, wasIncorrect: boolean) => {
        const hasChanged = scale.screenOrientation !== prevOrientation;
        if (!hasChanged) {
          return;
        }

        const isIncorrect = scale.incorrectOrientation && !wasIncorrect;
        if (isIncorrect) {
          this.showIncorrectOrientationMessage();
        } else {
          this.hideIncorrectOrientationMessage();
        }
      });
  }

  // ----------------------------------------------------------------------------------------------
  private initGroup(): Phaser.Group {
    const group = this.game.add.group();
    return (group);
  }

  // ----------------------------------------------------------------------------------------------
  private initGroups(): void {
    this._backgroundGroup = this.initGroup();
    this._entitiesGroup = this.initGroup();
    this._uiGroup = this.initGroup();
  }

  // ----------------------------------------------------------------------------------------------
  private initPlayerInputs(): void {
    this._playerInputs = new PlayerInputs(this.game, this._uiGroup, this.calcViewOffset());
    this._playerInputs.fireSignal.add(() => {
      this.fire();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private initSystems(): void {
    const game = this.gameObject;

    this._physicsSystem = new PhysicsSystem(game);
    this._gridSystem = new GridSystem(game);
    this._coinSpawnerSystem = new CoinSpawnerSystem(game, this._gridSystem, this._entitiesGroup);

    this._effectsSystem = new EffectsSystem(game, this.calcViewOffset(), this._entitiesGroup,
      this._uiGroup, this._audioPlayer);

    this._bumperSpawnerSystem = new BumperSpawnerSystem(game, this._gridSystem,
      this._effectsSystem, this._audioPlayer, this._entitiesGroup);

    this._basketScoringSystem = new BasketScoringSystem(game, this._effectsSystem,
      this._bumperSpawnerSystem);

    this._winUiSystem = new WinUiSystem(game, this._audioPlayer, this._uiGroup);
  }

  // ----------------------------------------------------------------------------------------------
  private initTimer(): void {
    this._timer = this.game.time.create(false);
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private initUi(): void {
    this.initBackground();
    this.initPlayerInputs();
  }

  // ----------------------------------------------------------------------------------------------
  private newBallOnRope(): void {
    const metrics = getRandomRopeMetrics();
    this._gameRope.newBall(metrics.length, metrics.swing, this._effectsSystem.isEasyModeActive)
      .then((success) => {
        if (success) {
          this._basketScoringSystem.nextWave();
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  }

  // ----------------------------------------------------------------------------------------------
  private nextWave(wasPrevWaveSuccessful?: boolean): void {
    this._lastBasketReboundTimeMs = 0;

    this._effectsSystem.nextWave(wasPrevWaveSuccessful);
    this._coinSpawnerSystem.nextWave();
    this._bumperSpawnerSystem.nextWave(wasPrevWaveSuccessful);

    if (wasPrevWaveSuccessful) {
      this._gameRope.swingDurationMs += this.config.ropeSwingDeltaDurationSecs * 1000;
    } else {
      this._gameRope.swingDurationMs = this.config.ropeSwingStartDurationSecs * 1000;
    }

    this.newBallOnRope();
  }

  // ----------------------------------------------------------------------------------------------
  private onBallBeginContact(otherBody: Phaser.Physics.P2.Body, _otherP2Body: p2.Body,
    _ballShape: p2.Shape, otherShape: p2.Shape, contactEquations: p2.ContactEquation[]): void {
    const sprite = otherBody ? otherBody.sprite : null;

    if (sprite instanceof Wall) {
      this.onBallBeginContactWall(sprite, contactEquations);
    } else if (sprite === this._ground) {
      this.onBallBeginContactGround();
    } else if (sprite === this._out) {
      this.onBallBeginContactOut();
    } else if (this._effectsSystem.basket.isBottomShape(otherShape)) {
      this.onBallBeginContactBottomBasket();
    } else if (sprite === this._effectsSystem.basket) {
      this.onBallBeginContactBasket(contactEquations);
    } else if (sprite instanceof Coin) {
      this.onBallBeginContactCoin(sprite);
    } else if (sprite instanceof Bumper) {
      this.onBallBeginContactBumper(sprite);
    } else if (sprite instanceof Spring) {
      this.onBallBeginContactSpring(sprite);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private onBallBeginContactBasket(contactEquations: p2.ContactEquation[]): void {
    this._effectsSystem.basket.bounce();

    // sometimes, the ball can "rattle" the basket, hitting it many times, when it only looked
    // like one hit. this helps to mitigate that
    const now = Date.now();
    const BASKET_REBOUND_THRESHOLD_TIME_MS = 100;
    if (now - this._lastBasketReboundTimeMs < BASKET_REBOUND_THRESHOLD_TIME_MS) {
      return;
    }

    this._lastBasketReboundTimeMs = now;

    this.ballRebounds('snd-ball-rebound-basket', contactEquations);
  }

  // ----------------------------------------------------------------------------------------------
  private onBallBeginContactBottomBasket(): void {
    this._effectsSystem.basket.bounce();

    const scoringData = this._basketScoringSystem.score();
    this._winUiSystem.present(scoringData);

    if (scoringData.isQuick) {
      UiFlash.Create(
        this.game,
        {
          numFlashes: 1,
          alpha: 0.9,
        },
        this.uiGroup,
      );
    }

    this._coinSpawnerSystem.ballScored();

    this._ball.destroy();

    this._effectsSystem.ball = null;

    this.nextWave(true);
  }

  // ----------------------------------------------------------------------------------------------
  private onBallBeginContactBumper(bumper: Bumper): void {
    bumper.bump(this._ball);
  }

  // ----------------------------------------------------------------------------------------------
  private onBallBeginContactCoin(coin: Coin): void {
    this.collectCoin(coin);
  }

  // ----------------------------------------------------------------------------------------------
  private onBallBeginContactGround(): void {
    this.ballDeff(true);
  }

  // ----------------------------------------------------------------------------------------------
  private onBallBeginContactOut(): void {
    this._ball.destroy();

    const OUT_OF_BOUNDS_DELAY_MS = 500;
    this._timer.add(OUT_OF_BOUNDS_DELAY_MS, () => {
      this.nextWave();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private onBallBeginContactSpring(spring: Spring): void {
    this._effectsSystem.spring(spring);
  }

  // ----------------------------------------------------------------------------------------------
  private onBallBeginContactWall(wall: Wall, contactEquations: p2.ContactEquation[]): void {
    if (wall.isSpiked) {
      this.ballDeff(false);
    } else {
      this.ballReboundsWall(contactEquations);
    }
  }

  // ----------------------------------------------------------------------------------------------
  /* tslint:disable:no-unused-variable */
  private presentLostLivesHeart(value: number): void {
    const heart = new LostLivesHeart(this.game, 400, 80 + this.calcViewOffset(), value);
    this.uiGroup.add(heart);
  }

  // ----------------------------------------------------------------------------------------------
  private presentScreenTremor(intensity: number): void {
    const DURATION = 350;
    this.camera.shake(intensity, DURATION);
  }

  // ----------------------------------------------------------------------------------------------
  private setBall(ball: Ball): void {
    this._ball = ball;
    this._ball.basketFront = this._effectsSystem.basket.front;

    this._ball.physicsBody.onBeginContact.add(this.onBallBeginContact, this);

    this._entitiesGroup.addAt(this._ball, this._entitiesGroup.getIndex(this._ball.basketFront));

    this._effectsSystem.ball = ball;
  }

  // ----------------------------------------------------------------------------------------------
  private showIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      return;
    }

    this.game.paused = true;
    this._incorrectOrientationImage = this.add.group();

    const graphics = this.add.graphics(0, 0, this._incorrectOrientationImage);
    graphics.beginFill(0, 1.0);
    graphics.drawRect(0, 0, this.game.width, this.game.height);
    graphics.endFill();

    const image = this.add.image(0, 0, 'incorrect-orientation-message', undefined,
      this._incorrectOrientationImage);
    image.anchor.set(0.5);
    image.x = this.game.width / 2;
    image.y = this.game.height / 2;

    image.width = this.game.width;
    image.scale.y = image.scale.x;

    if (image.height < this.game.height) {
      image.height = this.game.height
      image.scale.x = image.scale.y;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private startGame(): void {
    this._effectsSystem.basket.setNormScale();

    this.nextWave();

    listenerCallback(this.gameObject, Listeners.SET_SECONDS, this.config.startTimeInSeconds);
  }
}
